/**
 * Created by Komediruzecki on 7.1.2017..
 */


scopeFunctionShiftReg();

function scopeFunctionShiftReg() {
    let canvasReg = document.getElementById("shiftRegisters");
    let contextReg = canvasReg.getContext("2d");

    let currentSpeed = 50;
    let numOfFlips = 4;

    let stateReg = {
        simName: "Posmačni registar",
        // D0, D1, D2, D3, CLK, IMPL-SWITCH
        D: getArray(0, numOfFlips),
        Q: getArray(0, numOfFlips),
        nQ: getArray(1, numOfFlips),
        CP: 0,
        sIn: 0,
        edge: 0,
        hots: [],
        impl: 0,
        animationOffset: 0,
        animationPattern: [14, 6],
        scaleFactor: 1.7,
        flipsCount: {
            min: 1,
            max: 20,
            minInner: 1,
            maxInner: 8,
            resetTo: 4,
            numOfFlips: numOfFlips
        },
    };
    stateReg.cpId = 0;
    stateReg.sInId = 1;
    stateReg.implId = 2;
    stateReg.addFlipId = 3;

    let stateD = {
        simName: "Implementacija posmačnog registra",
        D: stateReg.D,
        CP: stateReg.CP,
        CPEdge: stateReg.CPEdge,
        Q: stateReg.Q,
        nQ: stateReg.nQ,
        userInput: stateReg.userInput,
        animationOffset: 0,
        animationPattern: [14, 6]
    };


    Object.defineProperties(stateD, {
        "setQ": {
            set: function (newVal) {
                stateD.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateD.nQ = newVal;
            }
        },
    });

    let min = 1.3;
    let max = 1.6;
    let scales = fillScales(min, max);
    let zoomHotsReg = {
        min: min,
        max: max,
        minReset: min,
        maxReset: max,
        blackBoxMin: getArray(min, 20),//.concat(scales.blackBoxMin),
        blackBoxMax: getArray(max, 15).concat(scales.blackBoxMax),
        innerMin: getArray(min, 7).concat(scales.innerImplMin),
        innerMax: getArray(max, 4).concat(scales.innerImplMax),
        standard: stateReg.scaleFactor,
        hots: []
    };


    function fillScales(min, max) {
        let scales = {};
        scales.blackBoxMax = fillArray(max, -0.02, 5);
        //scales.blackBoxMin = fillArray(min, -0.05, 5);
        scales.innerImplMax = fillArray(max, -0.08, 4);
        scales.innerImplMin = fillArray(min, -0.1, 1);
        return scales;
    }

    function setProperScales(numOfFlips, impl) {
        let newScaleFactor;
        switch (impl) {
            case 0:
                zoomHotsReg.min = zoomHotsReg.blackBoxMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.blackBoxMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            case 1:
                zoomHotsReg.min = zoomHotsReg.innerMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.innerMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            default:
                newScaleFactor = stateReg.scaleFactor;
        }
        setScaleFactor(newScaleFactor);
    }

    function checkFlipsCount(state, oldNumOfFlips) {
        let impl = state.impl;
        let reset = false;
        let newNumOfFlips = oldNumOfFlips;
        switch (impl) {
            case 0:
                if (newNumOfFlips < state.flipsCount.min) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;

                }
                if (newNumOfFlips > state.flipsCount.max) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
            case 1:
                if (newNumOfFlips < state.flipsCount.minInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                if (newNumOfFlips > state.flipsCount.maxInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
        }
        if (reset) {
            setDefaultScales();
        }
        setProperScales(newNumOfFlips, impl);
        return newNumOfFlips;
    }

    function setDefaultScales() {
        zoomHotsReg.max = zoomHotsReg.maxReset;
        zoomHotsReg.min = zoomHotsReg.minReset;
        let newScaleFactor = zoomHotsReg.standard;
        setScaleFactor(newScaleFactor);
    }


    let constReg = new Constants(stateReg.scaleFactor);
    saveClickHots(zoomHotsReg, 0, 0, canvasReg.width, canvasReg.height);

    function setScaleFactor(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function processZoom(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function startSimulation() {
        stateReg.animationOffset++;
        if (stateReg.animationOffset > 2000) {
            stateReg.animationOffset = 0;
        }
        stateD.animationOffset++;
        if (stateD.animationOffset > 2000) {
            stateD.animationOffset = 0;
        }
        update();
    }

    function updateState(state, newNumOfFlips) {
        let D = state.D;
        let Q = state.Q;
        let nQ = state.nQ;
        let oldNumOfFlips = state.flipsCount.numOfFlips;
        if (newNumOfFlips > oldNumOfFlips) {
            D = D.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            Q = Q.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            nQ = nQ.concat(getArray(1, newNumOfFlips - oldNumOfFlips));
        } else if (newNumOfFlips < oldNumOfFlips) {
            D = D.slice(0, newNumOfFlips);
            Q = Q.slice(0, newNumOfFlips);
            nQ = nQ.slice(0, newNumOfFlips);
        } else {
            // No changes
        }

        state.flipsCount.numOfFlips = newNumOfFlips;
        let newState = {
            simName: state.simName,
            // D0, D1, D2, D3, CLK, IMPL-SWITCH
            D: D,
            Q: Q,
            nQ: nQ,
            CP: state.CP,
            edge: state.edge,
            hots: state.hots,
            impl: state.impl,
            animationOffset: state.animationOffset,
            animationPattern: state.animationPattern,
            scaleFactor: state.scaleFactor,
            flipsCount: state.flipsCount,
            sIn: state.sIn
        };
        newState.cpId = 0;
        newState.sInId = 1;
        newState.implId = 2;
        newState.addFlipId = 3;
        return newState;
    }

    function clickHotEvent2(e) {
        let rect = canvasReg.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateReg.hots.length; i < l; i++) {
            let h = stateReg.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
                // We are inside click hot
                // TO-DO logic for user clicks
                switch (h.row) {
                    case stateReg.sInId:
                        stateReg.sIn = (stateReg.sIn === 1) ? 0 : 1;
                        break;
                    case stateReg.cpId:
                        // CP changed
                        animateFallingCP(stateReg);
                        break;
                    case stateReg.addFlipId:
                        let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                        let newNumOfFlips = calculateFlips(e, oldNumOfFlips);
                        let checkedFlipsCount =
                            checkFlipsCount(stateReg, newNumOfFlips);
                        stateReg = updateState(stateReg, checkedFlipsCount);
                        break;
                    case stateReg.implId:
                        let foundRightClick = false;
                        if ("which" in e) {
                            if (e.which === 3) {
                                foundRightClick = true;
                            }
                        } else if (e.button === 2) {
                            foundRightClick = true;
                        }
                        if (foundRightClick) {
                            propRightClick(e, stateReg, h);
                            let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                            let checkedFlipsCount =
                                checkFlipsCount(stateReg, oldNumOfFlips);
                            stateReg = updateState(stateReg, checkedFlipsCount);
                        }
                        break;
                    default:
                        alert("Impossible click!");
                }
                update();
                break;
            }
        }
    }

    function getArray(value, numOfElements) {
        let array = [];
        for (let i = numOfElements - 1; i >= 0; i--) {
            array.push(value);
        }
        return array;
    }

    function drawRegImpl(canvas, context, outerState, stateD, constants) {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateD.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateD);
        clearStateHots(stateReg);

        // Init colors
        let colorsIn = getColorStyle([stateReg.CP], constants);
        let colorsOut = getColorStyle(stateReg.Q, constants);
        let colorsLeft = getColorStyle([0, 0, stateReg.sIn], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        // Inner D flip flops
        let numOfFlips = outerState.flipsCount.numOfFlips;
        let scaleD = constants.scaleFactor - 0.5;
        let constD = new Constants(scaleD);
        let flipDWidth = 50 * constD.scaleFactor;
        let flipDHeight = 67 * constD.scaleFactor;
        let dist = 50 * constD.scaleFactor;
        let holes = numOfFlips - 1;
        // Out box for holding D flip flops
        let boxWidth = dist * (holes + 1) + 2 * constants.inputLineLengthD +
            flipDWidth * numOfFlips * (constants.scaleFactor - 0.5);
        let boxHeight = 110 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = -10 * constants.scaleFactor;
        let addSpace = 0;
        if (numOfFlips === 8) {
            addSpace += 35;
        }
        let regBox = {
            startX: (canvas.width - boxWidth) / 2 + xOff,
            startY: (canvas.height - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth + addSpace,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            boxLineWidth: constants.boxLineWidth,
            lineColors: [colorsIn, colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            strokeWidth: constants.boxStrokeWidth,
            state: stateReg,
            flipFlopType: 1,
            // Indexes are: 0, 1, 2, 3:
            // 0 and 1 for left and right, 2 and 3 for up and down
            // 0 - Draw from left to right and from up to down, 1 inverse of 0.
            lineDirection: [0, 0, 1, 1],
            clickText: [
                [outerState.CP],
                getArray(-1, numOfFlips),
                getArray(-1, 2).concat(outerState.sIn)
            ]
        };

        // Define Box Elements
        regBox.addLines = [
            getArray(1, 1),
            getArray(1, numOfFlips),
        ];
        regBox.addValues = [
            getArray(0, 1),
            getArray(1, numOfFlips),
        ];
        regBox.addClickBox = [
            getArray(true, 1),
            getArray(false, numOfFlips),
        ];

        regBox.clickIds = [
            [outerState.cpId],
            getArray(-1, numOfFlips),
        ];

        // Init text
        let letterOut = "Q";
        let textIn = ["CP"];
        let textOut = [];
        let unicodeSubScript0 = 8320;
        for (let c = 0; c < numOfFlips; c++) {
            // Make this generic...
            if (c < 10) {
                textOut.push(letterOut + String.fromCharCode(unicodeSubScript0 + c));
            } else if (c < 20) {
                let uni1 = 8321;
                let uni2 = unicodeSubScript0 + c - 10;
                textOut.push(letterOut +
                    String.fromCharCode(uni1) + String.fromCharCode(uni2));
            }
        }

        textOut = textOut.reverse();

        regBox.text = [
            textIn,
            textOut,
        ];

        regBox.textPos = [
            getArray(0, 1),
            getArray(0, numOfFlips),
        ];

        // addBoxElements(regBox, numOfFlips);
        let sInClickBox = [false, false, true];
        let sInValues = [0, 0, 1];
        let sInLines = [0, 0, 1];
        let sInTextPos = [0, 0, 0];
        let textLeft = ["", "", "Sin"];
        let sInClickIds = [-1, -1, stateReg.sInId];
        regBox.addValues.push(sInValues);
        regBox.addLines.push(sInLines);
        regBox.textPos.push(sInTextPos);
        regBox.addClickBox.push(sInClickBox);
        regBox.text.push(textLeft);
        regBox.clickIds.push(sInClickIds);

        // Save click hots for view change
        let regBoxClickId = outerState.implId;
        saveClickHots(outerState, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let inputPosition = 3;
        let outputPosition = 2;
        let inLineIndex = 0;
        let outLineIndex = 1;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, inLineIndex,
            inputPosition, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        // Draw Inner Clock
        drawFlipLines(context, regBox, 2, 0, constants);

        // Draw D Flip Flops
        let jumpAhead = (regBox.boxWidth -
            (numOfFlips * flipDWidth + holes * dist)) / 2;
        let yOffInner = (regBox.boxHeight - (flipDHeight)) / 2;
        let flipDDistance = dist;
        dist += flipDWidth;

        // Draw inner D flip flops
        // Draw CP Line
        drawCPLine(context, regBox, constants);

        for (let c = 0; c < numOfFlips; c++) {
            let curD = {
                xCoord: regBox.startX + jumpAhead + (dist * c),
                yCoord: regBox.startY + (yOffInner),
                state: stateD,
                boxWidth: flipDWidth,
                boxHeight: flipDHeight
            };
            drawD(c, context, regBox, curD, constD, flipDDistance);
        }

        // Draw decoder state
        let naturalOrder = 1;
        drawDecodedState(context, regBox, regBox.state, constants, naturalOrder);
    }


    function drawCPLine(context, box, constants) {
        // Inner D flip flops
        let colorsIn = getColorStyle([box.state.CP], constants);
        let numOfFlips = box.state.flipsCount.numOfFlips;
        let scaleD = constants.scaleFactor - 0.5;
        let constD = new Constants(scaleD);
        let flipDWidth = 50 * constD.scaleFactor;
        //let flipDHeight = 67 * constD.scaleFactor;
        let dist = 50 * constD.scaleFactor;
        let holes = numOfFlips - 1;
        let jumpAhead = (box.boxWidth -
            (numOfFlips * flipDWidth + holes * dist)) / 2;
        dist += flipDWidth;


        // Add CP inner line
        let yLineOff = getYLinesOutOff(box.boxHeight, 3);


        let connectCPToD = getLineParams(
            box.startX + box.boxWidth / 2,
            box.startY + box.boxHeight,
            box.startX + box.boxWidth / 2,
            box.startY + box.boxHeight - yLineOff
        );

        let innerCPLineLeft = getLineParams(
            box.startX + flipDWidth / 2 + jumpAhead,
            box.startY + box.boxHeight - yLineOff,
            connectCPToD.endX,
            connectCPToD.endY
        );

        let innerCPLineRight = getLineParams(
            innerCPLineLeft.endX,
            innerCPLineLeft.endY,
            box.startX + flipDWidth / 2 + jumpAhead + (dist * (numOfFlips - 1)),
            box.startY + box.boxHeight - yLineOff
        );

        drawComplexLine(context, innerCPLineLeft, colorsIn[0],
            box.state, constants, 1);
        drawComplexLine(context, innerCPLineRight, colorsIn[0],
            box.state, constants, 0);
        drawComplexLine(context, connectCPToD, colorsIn[0],
            box.state, constants, 1);

        let dotCP = {
            centerX: connectCPToD.endX,
            centerY: connectCPToD.endY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };
        drawCircle(context, dotCP);

    }

    function update() {
        if (stateReg.impl === 0) {
            drawReg(canvasReg, contextReg, stateReg, constReg);
        } else {
            drawRegImpl(canvasReg, contextReg, stateReg, stateD, constReg);
        }
    }

    function getColorStyle(listToCheck, constants) {
        let listLength = listToCheck.length;
        let colorsStlyes = [];
        for (let c = 0; c < listLength; c++) {
            if (listToCheck[c] === 1) {
                colorsStlyes.push(constants.greenLineActiveColor);
            } else if (listToCheck[c] === 0) {
                colorsStlyes.push(constants.passiveStateColor);
            }
        }
        return colorsStlyes;
    }

    function addClickAdder(context, box, constants) {
        let clickToAddFlip = {
            startX: 800,
            startY: 40,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            clickId: box.state.addFlipId,
            clickText: box.state.flipsCount.numOfFlips
        };
        drawClickBox(context, clickToAddFlip, box.state, constants);

    }

    function drawReg(canvas, context, state, constants) {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateReg);

        // Init colors
        let colorsIn = getColorStyle([state.CP], constants);
        let colorsOut = getColorStyle(state.Q, constants);
        let colorsLeft = getColorStyle([state.sIn], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        let numOfFlips = state.flipsCount.numOfFlips;
        let boxWidth = 40 + (numOfFlips * constants.clickBoxWidth) * constants.scaleFactor;
        let boxHeight = 60 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let regBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            flipFlopType: 2,
            lineDirection: [0, 0, 1, 1],
            clickText: [
                [state.CP],
                getArray(-1, numOfFlips),
                [state.sIn]
            ],
            lineColors: [colorsIn, colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            lineWidth: constants.boxStrokeWidth,
            state: state
        };

        let textLeft = ["Sin"];

        // Define Box Elements
        regBox.addLines = [
            getArray(1, 1),
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addValues = [
            getArray(0, 1),
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addClickBox = [
            getArray(true, 1),
            getArray(false, numOfFlips),
            getArray(true, 1),
        ];

        regBox.clickIds = [
            [state.cpId],
            getArray(-1, numOfFlips),
            [state.sInId]
        ];

        // Init text
        let letterOut = "Q";
        let textIn = ["CP"];
        let textOut = [];
        let unicodeSubScript0 = 8320;
        for (let c = 0; c < numOfFlips; c++) {
            // Make this generic...
            if (c < 10) {
                textOut.push(letterOut + String.fromCharCode(unicodeSubScript0 + c));
            } else if (c < 20) {
                let uni1 = 8321;
                let uni2 = unicodeSubScript0 + c - 10;
                textOut.push(letterOut +
                    String.fromCharCode(uni1) + String.fromCharCode(uni2));
            }
        }

        textOut = textOut.reverse();

        regBox.text = [
            textIn,
            textOut,
            textLeft
        ];

        regBox.textPos = [
            getArray(0, 1),
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];

        // Save click hots for view change
        let regBoxClickId = state.implId;
        saveClickHots(state, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let outputPosition = 2;
        let clkPosition = 0;
        let outLineIndex = 1;
        let leftLineIndex = 2;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, 0, 3, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        drawFlipLines(context, regBox, leftLineIndex,
            clkPosition, constants);

        // Additional items

        // Draw decoder state
        let naturalOrder = 1;
        drawDecodedState(context, regBox, regBox.state, constants, naturalOrder);
    }

    function wheelEventHandling(e) {
        myMouseWheelEvent(e, canvasReg, zoomHotsReg);
        update();
    }

    function myMouseWheelEvent(e, canvas, hots) {
        e.preventDefault();
        mouseZoomer(e, canvas, hots, processZoom, stateReg.scaleFactor);
    }

    function propRightClick(e, state, h) {
        if (h.row === state.implId) {
            state.impl = (state.impl === 1) ? 0 : 1;
            setScaleFactor(state.scaleFactor);
            e.preventDefault();
        }
    }

    function rightClickHot(e) {
        e.preventDefault();
    }

    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEvent2);
        canvas.addEventListener('mousewheel', wheelEventHandling, false);
        canvas.addEventListener('contextmenu', rightClickHot, false);

        // Remove dblclk on canvas to select text in document.
        canvas.addEventListener('selectstart',
            function (e) {
                e.preventDefault();
                return false;
            }, false);
        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', wheelEventHandling, false)

    }

    function init(canvas, context, state, constants) {
        if (state.impl === 0) {
            drawReg(canvas, context, state, constants);
        } else {
            drawRegImpl(canvasReg, contextReg, stateReg, stateD, constReg);
        }
    }

    function getYLinesInnerOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }

    function getYLinesOutOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }

    function drawQToDLine(ID, context, outBox, args, constants) {
        let colorQ = getColorStyle([args.state.Q[ID]], constants);
        let colorQM1 = getColorStyle([args.state.Q[ID - 1]], constants);
        let colorCP = getColorStyle([args.state.CP], constants);

        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let yOff = 35 * constants.scaleFactor;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff - yOff,
            constants.passiveStateColor
        );

        let outQLeft1 = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[1].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[1].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.passiveStateColor
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.passiveStateColor
        );

        let yOffCP = getYLinesOutOff(outBox.boxHeight, outBox.text[2].length);
        let addVal = getCPComplementFix(2, constants);
        let outCPLine = getLineParams(
            args.startX + args.boxWidth / 2,
            outBox.startY + outBox.boxHeight - yOffCP,
            args.startX + args.boxWidth / 2,
            args.startY + args.boxHeight - addVal,
            constants.passiveStateColor
        );

        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft1, colorQ[0], args.state, constants, 0);
        drawComplexLine(context, outCPLine, colorCP[0], args.state, constants, 1);

        let inDLeft = getLineParams(
            args.startX - args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX - args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        drawComplexLine(context, inDLeft, colorQM1[0], args.state, constants, 0);

        let dotCP = {
            centerX: inDLeft.startX,
            centerY: inDLeft.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

        let dotD = {
            centerX: outCPLine.startX,
            centerY: outCPLine.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

        drawCircle(context, dotD);
        drawCircle(context, dotCP);
    }

    function drawSinLine(ID, context, outBox, args, constants) {
        let colorSin = getColorStyle([stateReg.sIn], constants);
        let colorQ = getColorStyle([args.state.Q[ID]], constants);
        let colorCP = getColorStyle([args.state.CP], constants);

        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let yOff = 35 * constants.scaleFactor;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff - yOff,
            constants.passiveStateColor
        );

        let outQLeft1 = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[1].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[1].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.passiveStateColor
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.passiveStateColor
        );

        let yOffCP = getYLinesOutOff(outBox.boxHeight, outBox.text[2].length);
        let addVal = getCPComplementFix(2, constants);
        let outCPLine = getLineParams(
            args.startX + args.boxWidth / 2,
            outBox.startY + outBox.boxHeight - yOffCP,
            args.startX + args.boxWidth / 2,
            args.startY + args.boxHeight - addVal,
            constants.passiveStateColor
        );

        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft1, colorQ[0], args.state, constants, 0);
        drawComplexLine(context, outCPLine, colorCP[0], args.state, constants, 1);

        let yLinesOutOffH = getYLinesOutOff(outBox.boxHeight, outBox.text[2].length);
        let inDDown = getLineParams(
            args.startX - args.lineLength,
            outBox.startY + outBox.boxHeight - yLinesOutOffH,
            args.startX - args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        let inDLeft = getLineParams(
            inDDown.startX,
            inDDown.startY,
            outBox.startX,
            inDDown.startY,
            constants.passiveStateColor
        );

        drawComplexLine(context, inDDown, colorSin[0], args.state, constants, 1);
        drawComplexLine(context, inDLeft, colorSin[0], args.state, constants, 0);

        let dotCP = {
            centerX: outCPLine.startX,
            centerY: outCPLine.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };
        drawCircle(context, dotCP);
    }

    function drawD(ID, context, outBox, args, constants, dist) {

        // Init Colours
        stateD.D = outBox.state.D;
        stateD.CP = outBox.state.CP;
        stateD.Q = outBox.state.Q;
        stateD.nQ = outBox.state.nQ;
        stateD.userInput = outBox.state.userInput;
        stateD.sIn = outBox.state.sIn;

        let colorsIn;
        if (ID === 0) {
            colorsIn = getColorStyle([stateD.sIn], constants);
        } else {
            colorsIn = getColorStyle([stateD.Q[ID - 1]], constants);
        }
        let colorsOut = getColorStyle([stateD.Q[ID], stateD.nQ[ID]], constants);
        let colorsBox = {box: constants.colors.flipD, clickBox: "#746dc5"};

        let boxWidth = args.boxWidth;
        let boxHeight = args.boxHeight;
        let xOff = args.xCoord;
        let yOff = args.yCoord;
        let genericBox = {
            startX: xOff,
            startY: yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthD,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [
                [-1, -1],
                [-1, -1],
            ],
            textPos: [[1, 1], [1, 1]],
            addLines: [[1, 0], [1, 1]],
            lineDirection: [0, 0, 0, 1],
            addValues: [[1, 0], [1, 1]],
            addClickBox: [
                [false, false, false, false],
                [false, false, false, false]
            ],
            text: [["D", ""], ["Q", "Q'"]],
            lineColors: [colorsIn, colorsOut],
            flipFlopType: 2,
            boxLineWidth: constants.boxLineWidth,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: stateD,
            flipDDistance: dist

        };

        // Draw flip flop. Draw IO lines
        passiveStyle(context, constants);
        drawFlipFlopBox(context, genericBox, constants);
        drawFlipLines(context, genericBox, 0, 0, constants);
        drawFlipLines(context, genericBox, 1, 1, constants);
        drawCPBelow(context, genericBox, constants, genericBox.flipFlopType);
        if (ID === 0) {
            drawSinLine(ID, context, outBox, genericBox, constants);
        } else {
            drawQToDLine(ID, context, outBox, genericBox, constants);
        }
        passiveStyle(context, constants);
    }

    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateFallingCP(state) {
        state.CP = (state.CP === 1) ? 0 : 1;
        if (state.CP === 0) {
            state.CPEdge = 1;
        } else {
            state.CPEdge = 0;
        }

        let tempQ = [];
        for (let i = 0; i < state.Q.length; i++) {
            tempQ.push(state.Q[i]);
        }
        if (state.CPEdge === 1) {
            for (let c = 0; c < state.Q.length; c++) {
                if (c === 0) {
                    state.Q[c] = state.sIn;
                    state.nQ[c] = (state.Q[c] === 1) ? 0 : 1;
                } else {
                    let newQ = (tempQ[c - 1] === 1) ? 1 : 0;
                    let newNQ = (newQ === 1) ? 0 : 1;
                    state.Q[c] = newQ;
                    state.nQ[c] = newNQ;

                }
            }
        }
        state.CPEdge = 0;
    }

    init(canvasReg, contextReg, stateReg, constReg);
    eventListeners(canvasReg);
    setInterval(startSimulation, currentSpeed);
}