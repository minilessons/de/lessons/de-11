/*
 /!**
 * Created by Komediruzecki on 15.1.2017..
 *!/
 /!**
 * Created by Komediruzecki on 7.1.2017..
 *!/

 /!* Header *!/

 //////////////////////////////////////////////////////////////////////////


 /!**
 * Constants which can be scaled with scale factor.
 * This constructor holds default values for all constants, but all can change if user wants.
 *!/
 class Constants {
 constructor(/!**
 * @param scaleFactor scale factor to scale all components
 * @param boxHeight standard box height (default=50)
 * @param boxWidth standard box width
 * @param lineLength length of AND logic gate lines
 * @param IEEEArcRadius radius for AND logic gate
 * @param inputLineLength length of input lines for logic gates
 * @param lineWidthIO width of input and output lines for logic gates
 * @param inputLineDistance distance from input lines on logic gates
 * @param IEEEComplementRadius complement radius for complementing inputs and outputs
 * @param IEEEComplementDiameter complement diameter
 * @param IEEEConnectDotRadius radius for little black circle for joining wires
 * @param IEEEConnectDiameter connect dot diameter
 * @param clickBoxWidth width of a click box
 * @param clickBoxHeight height of a click box
 * @param constBoxWidth width for constant boxes
 * @param constBoxHeight height for constants boxes
 * @param outLineLength length of line that every logic gate extends after output line
 * @param verticalUpLength length of offset from logic gates
 * @param zeroAngle standard angle for drawing full circle (value=0)
 * @param fullCircle standard full angle for drawing circles (value=360)
 * @param varTextSize size of text for commenting inputs and outputs of logic gates
 *!/
 scaleFactor = 1,
 boxHeight = 50,
 boxWidth = 50,
 lineLength = 20,
 IEEEArcRadius = 20,
 inputLineLength = 15,
 lineWidthIO = 2,
 inputLineDistance = 20,
 IEEEComplementRadius = (Math.PI + 0.5),
 IEEEConnectDotRadius = (Math.PI - 1),
 clickBoxWidth = 15,
 clickBoxHeight = 15,
 constBoxWidth = 15,
 constBoxHeight = 15,
 outLineLength = 50,
 verticalUpLength = 50,
 zeroAngle = 0,
 fullCircle = 360,
 varTextSize = 24,
 CPLineWidth = 4,
 textLineWidth = 2,
 connectDotLineWidth = 1,
 complementLineWidth = 1,
 constBoxLineWidth = 1,
 connectLinesWidth = 2,
 clickBoxLineWidth = 1,
 complementColor = "black",
 IECFontSize = 21,
 standardLineColor = "black",
 textBoxWidth = 20,
 textBoxHeight = 20,
 blueTextColor = "blue",
 commonFillStyle = "#83A59E",
 commonTextOffset = 5,
 universalBoxWidth = 40,
 universalBoxHeight = 40,
 currentStateColor = "#FF1953",
 activeStateColor = "#962D8D",
 passiveStateColor = "black",
 redLineActiveColor = "red",
 simStyle = "#000000",
 simTextSize = "bold 16px Courier",
 simTextFont = "19px Courier",
 schemaComplementRadius = (Math.PI - 0.5) * 1.3,
 overLineLength = 8,
 clickHotsOffset = 10,
 currentLineWidth = 1.5,
 currentColor = "#41f547",
 complementFill = "#ffffff",
 frameColor = "#319666",
 frameBlacked = "#131212",
 backgroundFrameWidth = "2px solid black",
 stateDiagramFont = "12px Courier",
 IECTextFont = 21,
 timeTextFont = "14px Courier",
 delayTextColor = "#96396a",
 timeTextSize = 14,
 tubeLineWidth = 2,
 greenLineActiveColor = "#41f547",
 fontSizeIOValues = "12px",
 standardFont = " Courier",
 valuesBoxWidth = 5,
 valuesBoxHeight = 7,
 inverterComplementRadius = (Math.PI - 1),
 colors = {
 black: "black",
 yellow: "yellow",
 red: "red",
 white: "white",
 orange: "orange",
 awesome: "#8836ba",
 gray: "#AF1E37",
 darkRed: "#d02b2a",
 andColor: "#ABB7B7",
 purple: "#8e44ad",
 emerald: "#2ecc71",
 midnight: "#2c3e50",
 dodgerBlue: "#1E90FF",
 flipD: "#1E90FF",
 flipT: "#886F98",
 diagramArrow: "#da0000",
 flipSR: "#CB3A34",
 flipJK: "#2ecc71",
 registerColor: "#50928c",
 clickBoxColor: "#746dc5",
 darkBlue: "#00008B",
 decoderColor: "#d4d4d6",
 decoderTextColor: "#00017f",
 lightSkyBlue: "#87CEFA"
 },
 flipFontSize = 10,
 flipFontSizeInner = 14,
 flipFont = "px New Century Schoolbook",
 inputLineLengthNew = 21,
 registerFontSize = 10,
 registerFont = "px Century Schoolbook",
 boxStrokeWidth = 1.6,
 flipTextBoxWidth = 15,
 flipTextBoxHeight = 15,
 overlineWidth = 0.4,
 raisingEdgeWidth = 0.8,
 inputLineLengthD = 14,
 complementFillStyle = "#c2cfee",
 decoderBoxWidth = 80,
 decoderBoxHeight = 25,
 decoderFontSize = 12) {
 /!** Defines scale to use on all constants. *!/
 this.scaleFactor = scaleFactor;
 /!**
 * Standard IEEE box height
 * @type {number} value of boxHeight
 *!/
 this.boxHeight = boxHeight * scaleFactor;
 /!**
 * Standard IEEE box width
 * @type {number} value of boxWidth
 *!/
 this.boxWidth = boxWidth * scaleFactor;

 /!**
 * Length of line that extend arc in IEEE AND box
 * @type {number}
 *!/
 this.lineLength = lineLength * scaleFactor;
 /!**
 * Standard radius of arc that is used for AND logic gates
 * @type {number} value of arc radius
 *!/
 this.IEEEArcRadius = IEEEArcRadius * scaleFactor;
 /!**
 * Height of AND logic gate box
 * @type {number} value of box Height
 *!/
 this.NANDBoxHeight = 2 * this.IEEEArcRadius;
 /!**
 * Width of AND logic gate box
 * @type {number} value of box width
 *!/
 this.NANDBoxWidth = this.IEEEArcRadius + this.lineLength;

 /!**
 * Line length of inputs in logic gate box
 * @type {number} value of length
 *!/
 this.inputLineLength = inputLineLength * scaleFactor;

 /!**
 * Line width for input and output logic boxes
 * @type {number} value of line width
 *!/
 this.lineWidthIO = lineWidthIO * scaleFactor;

 /!**
 *
 * @type {number}
 *!/
 this.inputLineDistance = inputLineDistance * scaleFactor;

 /!**
 * Standard IEEE radius of a complement circle
 * @type {number} value of radius
 *!/
 this.IEEEComplementRadius = IEEEComplementRadius * scaleFactor;
 /!**
 * Standard IEEE diameter for complement circle
 * @type {number} value of diameter
 *!/
 this.IEEEComplementDiameter = 2 * this.IEEEComplementRadius;

 /!**
 * Standard IEEE radius of connect dot for joining lines
 * @type {number} value of radius
 *!/
 this.IEEEConnectDotRadius = IEEEConnectDotRadius * scaleFactor;

 /!**
 * Standard click box height for clickable boxes beside logic gates
 * @type {number} value of height of a click box
 *!/
 this.clickBoxHeight = clickBoxHeight * scaleFactor;
 /!**
 * Standard click box width for clickable boxes beside logic gates
 * @type {number} value of width of a click box
 *!/
 this.clickBoxWidth = clickBoxWidth * scaleFactor;

 /!**
 * Standard constant box height for constants on input of logic gates
 * @type {number} value of height of a constant box
 *!/
 this.constBoxWidth = constBoxWidth * scaleFactor;

 /!**
 * Standard constant box width for constants on input of logic gates
 * @type {number} value of width of a constant box
 *!/
 this.constBoxHeight = constBoxHeight * scaleFactor;

 /!**
 * Length of a line that goes out of logic gate box
 * @type {number} value of length
 *!/
 this.outLineLength = outLineLength * scaleFactor;

 /!**
 * Length of vertical line that connects returning line
 * and middle of logic gate
 * @type {number} line length
 *!/
 this.verticalUpLength = verticalUpLength * scaleFactor;

 /!**
 * Constant angle for drawing circle
 * @type {number} value: 0
 *!/
 this.zeroAngle = zeroAngle;

 /!**
 * Constant angle for drawing full circle
 * @type {number} value: 360
 *!/
 this.fullCircle = fullCircle;

 /!**
 * Text size standard for writing beside logic gate boxes
 * @type {number}
 *!/
 this.varTextSize = varTextSize * scaleFactor * 0.7;

 this.textBoxWidth = textBoxWidth * scaleFactor;
 this.textBoxHeight = textBoxHeight * scaleFactor;
 this.complementColor = complementColor;
 this.CPLineWidth = CPLineWidth;
 this.textLineWidth = textLineWidth;
 this.connectDotLineWidth = connectDotLineWidth;
 this.complementLineWidth = complementLineWidth;
 this.constBoxLineWidth = constBoxLineWidth;
 this.connectLinesWidth = connectLinesWidth;
 this.clickBoxLineWidth = clickBoxLineWidth;
 this.IECFontSize = IECFontSize * scaleFactor;
 this.standardLineColor = standardLineColor;
 this.blueTextColor = blueTextColor;
 this.commonFillStyle = commonFillStyle;
 this.commonTextOffset = commonTextOffset;

 this.universalBoxWidth = universalBoxWidth * scaleFactor;
 this.universalBoxHeight = universalBoxHeight * scaleFactor;
 this.currentStateColor = currentStateColor;
 this.activeStateColor = activeStateColor;
 this.redLineActiveColor = redLineActiveColor;
 this.passiveStateColor = passiveStateColor;

 this.simStyle = simStyle;
 this.simTextSize = simTextSize;
 this.simTextFont = simTextFont;

 this.schemaComplementRadius = schemaComplementRadius * scaleFactor;
 this.overLineLength = overLineLength * scaleFactor;
 this.clickHotsOffset = clickHotsOffset;
 this.currentLineWidth = currentLineWidth * scaleFactor;
 this.currentColor = currentColor;
 this.complementFill = complementFill;
 this.frameColor = frameColor;
 this.frameBlacked = frameBlacked;
 this.backgroundFrameWidth = backgroundFrameWidth;
 this.stateDiagramFont = stateDiagramFont;
 this.IECTextFont = (IECTextFont * scaleFactor) + "px Courier";
 this.timeTextFont = timeTextFont;
 this.delayTextColor = delayTextColor;
 this.timeTextSize = timeTextSize;
 this.tubeLineWidth = tubeLineWidth * scaleFactor;
 this.greenLineActiveColor = greenLineActiveColor;
 this.fontSizeIOValues = fontSizeIOValues;
 this.standardFont = standardFont;
 this.valuesBoxWidth = valuesBoxWidth * scaleFactor;
 this.valuesBoxHeight = valuesBoxHeight * scaleFactor;
 this.inverterComplementRadius = inverterComplementRadius * scaleFactor;
 this.colors = colors;
 this.flipFontSize = flipFontSize * scaleFactor;
 this.flipFont = flipFont;
 this.flipFontSizeInner = flipFontSizeInner * scaleFactor;
 this.inputLineLengthNew = inputLineLengthNew * scaleFactor;
 this.registerFont = registerFont;
 this.registerFontSize = registerFontSize * scaleFactor;
 this.boxStrokeWidth = boxStrokeWidth * scaleFactor;
 this.flipTextBoxWidth = flipTextBoxWidth * scaleFactor;
 this.flipTextBoxHeight = flipTextBoxHeight * scaleFactor;
 this.overlineWidth = overlineWidth * scaleFactor;
 this.raisingEdgeWidth = raisingEdgeWidth * scaleFactor;
 this.inputLineLengthD = inputLineLengthD * scaleFactor;
 this.complementFillStyle = complementFillStyle;
 this.decoderBoxWidth = decoderBoxWidth * scaleFactor;
 this.decoderBoxHeight = decoderBoxHeight * scaleFactor;
 this.decoderFontSize = decoderFontSize * scaleFactor;
 }
 }


 class usedUnicode {
 constructor() {
 /!* Subscript <sub> x </sub> numbers *!/
 this.sup0 = '\u2070';
 this.sup1 = '\u00B9';
 this.sup2 = '\u00B2';
 this.sup3 = '\u00B3';
 this.sup4 = '\u2074';
 this.sup5 = '\u2075';
 this.sup6 = '\u2076';
 this.sup7 = '\u2077';
 this.sup8 = '\u2078';
 this.sup9 = '\u2079';

 /!* Superscript <sup> x </sup> numbers *!/
 this.sub0 = '\u2080';
 this.sub1 = '\u2081';
 this.sub2 = '\u2082';
 this.sub3 = '\u2083';
 this.sub4 = '\u2084';
 this.sub5 = '\u2085';
 this.sub6 = '\u2086';
 this.sub7 = '\u2087';
 this.sub8 = '\u2088';
 this.sub9 = '\u2089';

 // Letters
 this.subA = '\u2090';
 this.subE = '\u2091';
 this.subO = '\u2092';
 this.subM = '\u2098';
 this.subN = '\u2099';

 this.supN = '\u207F';

 // Other signs
 this.subPlus = '\u208A';
 this.subMinus = '\u208B';
 this.subBracketLeft = '\u208F';
 this.supBracketLeft = '\u207D';
 this.subBracketRight = '\u208E';
 this.supBracketRight = '\u207E';
 }
 }


 /!**
 *
 * @param x coordinate for clickBox
 * @param y coordinate for clickBox
 * @param w width of clickBox
 * @param h height of clickBox
 * @param row index for userInput
 * @constructor
 *!/
 function Handler(x, y, w, h, row) {
 this.x = x;
 this.y = y;
 this.w = w;
 this.h = h;
 this.row = row;
 }

 function binaryToDecimal(sequenceBits) {
 let num = 0;
 for (let c = 0, l = sequenceBits.length; c < l; c++) {
 num += sequenceBits[c] * Math.pow(2, l - c - 1);
 }
 return num;
 }

 function writeMultilineText(canvas, context, multiLineText, constants) {
 let rect = canvas.getBoundingClientRect();
 let lineHeight = 20;
 let lines = multiLineText.split("\n");

 let yOffsetFromBoundingRect = 5;
 let textOff = lines.length * lineHeight + yOffsetFromBoundingRect;
 let startX = rect.width / 2;
 let startY = rect.height - textOff;

 context.beginPath();
 context.strokeStyle = constants.simStyle;
 context.font = constants.simTextFont;
 context.fillStyle = constants.simStyle;
 context.textAlign = "center";
 context.textBaseline = "middle";
 context.clearRect(1, startY - 20, canvas.width - 2, canvas.height - startY + 19);
 for (let i = 0, l = lines.length; i < l; i++) {
 context.fillText(lines[i], startX, startY + (i * lineHeight));
 }
 }
 /!*

 function writeMultilineText2(context, args, lineHeight, multiLineText, centered = true) {
 let lines = multiLineText.split("\n");

 let x1 = args.leftVertexX;
 let y1 = args.leftVertexY;
 let w = args.boxWidth;
 let h = args.boxHeight;

 // Set text in the center
 let textX = (centered === true) ? x1 + (w / 2) : x1;
 let textY = (centered === true) ? y1 + (h / 2) : y1;

 context.beginPath();
 context.font = args.textFont;
 context.textAlign = "center";
 context.textBaseline = "middle";
 context.fillStyle = args.fillStyle;
 context.strokeStyle = args.color;
 context.lineWidth = args.lineWidth;
 for (let i = 0, l = lines.length; i < l; i++) {
 context.fillText(lines[i], textX, textY + (i * lineHeight));
 //context.strokeText(lines[i], textX, textY + (i * lineHeight));
 }
 }
 *!/


 function writeSimulationName(canvas, context, name, constants) {
 writeMultilineText(canvas, context, name, constants);
 }

 function toDegrees(radian) {
 return (radian * 180) / Math.PI;
 }
 function toRadians(degree) {
 return (Math.PI / 180) * degree;
 }

 /!**
 * This function saves hot spots for <code>mousedown</code> and  <code>mouseup</code> events.
 * @param state box state which will be used to save hots
 * @param x coordinate of clickBox
 * @param y coordinate of clickBox
 * @param w width of clickBox
 * @param h height of clickBox
 * @param row index for userInput
 *!/
 function saveClickHots(state, x, y, w, h, row) {
 state.hots.push(new Handler(x, y, w, h, row));
 }


 function mouseZoomer(e, canvas, zoomHots, functionToCall, oldScaleFactor) {
 let rect = canvas.getBoundingClientRect();
 let pos = {
 x: e.clientX - rect.left,
 y: e.clientY - rect.top
 };
 //let oldScaleFactor = state.scaleFactor;
 // Check if mouse is in area which we consider..
 for (let i = 0, l = zoomHots.hots.length; i < l; i++) {
 let h = zoomHots.hots[i];
 if (pos.x >= h.x && pos.x < h.x + h.w &&
 pos.y >= h.y && pos.y < h.y + h.h) {
 // cross-browser wheel delta
 e = window.event || e; // old IE support
 let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
 let newScaleFactor = Math.max(zoomHots.min, Math.min(zoomHots.max,
 oldScaleFactor + (0.1 * delta)));
 functionToCall(newScaleFactor);
 e.returnValue = false;
 break;
 }
 }
 }


 /!**
 * Draws simple circle for making little "Complement" figure on each inverter.
 * @param context context to draw lines to
 * @param args arguments are as follows,
 *          startX x coordinate for circle center
 *          startY y coordinate for circle center
 *          radius radius of a circle
 *          startAngle angle at which circle is drawn
 *          endAngle representing value for whole circle (basically 360)
 *!/
 function drawCircle(context, args) {
 context.beginPath();
 context.strokeStyle = args.color;
 context.fillStyle = args.fillStyle;
 context.lineWidth = args.lineWidth;
 context.arc(args.centerX, args.centerY, args.radius, args.startAngle, args.endAngle, args.rotation);
 context.fill();
 context.stroke();
 }

 function drawFlipFlopBox(context, args, constants) {

 context.beginPath();
 context.rect(args.startX, args.startY,
 args.boxWidth, args.boxHeight);
 context.fillStyle = args.fillStyle;
 context.lineWidth = args.lineWidth;
 context.fill();
 context.strokeStyle = constants.colors.black;
 context.stroke();
 }

 /!**
 * Function sets background rectangle box in given canvas.
 * @param context context for drawing frame
 * @param width of context which frame goes to
 * @param height of context which frame goes to
 *!/
 function setBackgroundFrame(context, width, height, constants) {
 context.beginPath();
 context.strokeStyle = "#000000";
 context.strokeWidth = constants.backgroundFrameWidth;
 context.rect(0, 0, width, height);
 context.stroke();
 }


 function clearLineCanvas(context, line, width) {
 let x = (line.endX < line.startX) ? line.endX : line.startX;
 let y = (line.endY < line.startY) ? line.endY : line.startY;
 let w = Math.abs(line.endX - line.startX);
 let h = Math.abs(line.endY - line.startY);

 if (w === 0) {
 w = width;
 x -= (width / 2);
 } else if (h === 0) {
 h = width;
 y -= (width / 2);
 }
 context.clearRect(x, y, w, h);
 }


 /!**
 * Draws simple line from (x1, y1) to (x2, y2).
 * @param context context to draw line to
 * @param args arguments that holds line coordinats (generated from getLineParams method)
 * @param width width of line
 * @param pos 0 means to draw line from left to right and up to down, 1 means inverse
 *!/
 function drawConnectLine(context, args, width, pos = 0) {
 let x1 = args.startX;
 let y1 = args.startY;
 let x2 = args.endX;
 let y2 = args.endY;

 let l = {};
 if (pos === 0) {
 l.startX = (x1 > x2) ? x2 : x1;
 l.startY = (y1 > y2) ? y2 : y1;
 } else if (pos === 1) {
 l.startX = (x1 < x2) ? x2 : x1;
 l.startY = (y1 < y2) ? y2 : y1;
 }
 l.endX = (l.startX === x2) ? x1 : x2;
 l.endY = (l.startY === y2) ? y1 : y2;

 context.beginPath();
 context.strokeStyle = args.color;
 context.lineWidth = width;
 context.moveTo(l.startX, l.startY);
 context.lineTo(l.endX, l.endY);
 context.stroke();
 }


 function passiveStyle(context, constants) {
 context.strokeStyle = constants.passiveStateColor;
 context.lineWidth = constants.tubeLineWidth;
 context.setLineDash([]);
 }

 function activeStyle(context, state) {
 context.setLineDash(state.animationPattern);
 context.lineDashOffset = -state.animationOffset;
 }


 function drawValues(context, args, text, constants) {
 let textOut = text;
 if (text != 1 && text != 0) {
 textOut = (text === constants.greenLineActiveColor) ? 1 : 0;
 }

 let box = {
 leftVertexX: args.startX,
 leftVertexY: args.startY,
 boxWidth: constants.valuesBoxWidth,
 boxHeight: constants.valuesBoxHeight,
 color: constants.passiveStateColor,
 textFont: constants.fontSizeIOValues + constants.standardFont,
 fillStyle: constants.passiveStateColor,
 lineWidth: constants.textLineWidth

 };

 drawText(context, box, textOut, true, true);
 }


 function clickHotEvent(e, canvas, state, prop) {
 let rect = canvas.getBoundingClientRect();
 let pos = {
 x: e.clientX - rect.left,
 y: e.clientY - rect.top
 };

 // Check if click was in area which we consider
 for (let i = 0, l = state.hots.length; i < l; i++) {
 let h = state.hots[i];
 if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
 && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
 // We are inside click hot
 prop(e, state, h);
 break;
 }
 }
 }

 function drawOverlineText2(context, args, text, lineLength, constants) {
 args.text = text;
 drawFlipText(context, args, constants, true, false, 1);
 // Add overline
 let overClose = 1.6 * constants.scaleFactor;
 let xOff = 2.8 * constants.scaleFactor;
 let startX = args.startX + xOff;
 let startY = args.startY + overClose;
 let endX = startX + lineLength;
 let endY = startY;
 context.beginPath();
 context.lineWidth = constants.overlineWidth;
 context.moveTo(startX, startY);
 context.lineTo(endX, endY);
 context.stroke();
 }

 function drawComplementIndicator(context, xStart, yStart, constants, pos) {
 let params = {
 centerX: xStart,
 centerY: yStart + constants.schemaComplementRadius,
 radius: constants.schemaComplementRadius,
 startAngle: toRadians(0),
 endAngle: toRadians(360),
 color: constants.complementColor,
 fillStyle: constants.complementFillStyle,
 lineWidth: constants.complementLineWidth,
 rotation: true
 };
 drawCircle(context, params);
 }

 function drawEdgeIndicator(context, xStart, yStart,
 constants, orientation = 0) {
 let side = 8 * constants.scaleFactor;
 let height = 7 * constants.scaleFactor;
 let moveX, moveY;
 let x1, y1, x2, y2;
 if (orientation === 0) {
 moveX = xStart - side;
 moveY = yStart;
 x1 = xStart;
 y1 = yStart - height;
 x2 = xStart + side;
 y2 = yStart;

 } else {
 moveX = xStart;
 moveY = yStart - side;
 x1 = xStart + height;
 y1 = yStart;
 x2 = xStart;
 y2 = yStart + side;
 }

 context.beginPath();
 context.lineWidth = constants.raisingEdgeWidth;
 context.moveTo(moveX, moveY);
 context.lineTo(x1, y1);
 context.lineTo(x2, y2);
 context.stroke();
 }

 function drawCPBelow(context, box, constants, typeSpec = 1, yValueOff = 0) {
 // Calc common values
 let xStart = box.startX + (box.boxWidth / 2);
 let yStart = box.startY + box.boxHeight;
 let yPadInner = 15 * constants.scaleFactor;
 if (typeSpec === 0) {
 drawComplementIndicator(context, xStart, yStart, constants);
 yStart += constants.schemaComplementRadius * 2;
 yPadInner += constants.schemaComplementRadius * 2;
 } else if (typeSpec === 2) {
 drawComplementIndicator(context, xStart, yStart, constants);
 drawEdgeIndicator(context, xStart, yStart, constants);
 yStart += constants.schemaComplementRadius * 2;
 yPadInner += constants.schemaComplementRadius * 2;
 } else if (typeSpec === 3) {
 drawEdgeIndicator(context, xStart, yStart, constants);
 }

 let xOff = 12 * constants.scaleFactor;
 let yOff = yValueOff * constants.scaleFactor;
 let value = (box.state.CP === 1) ? 1 : 0;
 let textBox = {
 leftVertexX: xStart - xOff,
 leftVertexY: yStart + yOff,
 boxWidth: constants.valuesBoxWidth,
 boxHeight: constants.valuesBoxHeight,
 color: constants.passiveStateColor,
 textFont: constants.fontSizeIOValues + constants.standardFont,
 fillStyle: constants.passiveStateColor,
 lineWidth: constants.textLineWidth

 };
 drawText(context, textBox, value, true);
 // Add CP text
 let staticCP = "CP";

 // Text Style:
 context.font = constants.registerFontSize + constants.registerFont;
 context.textAlign = "center";
 context.textBaseline = "middle";
 context.fillStyle = constants.passiveStateColor;
 context.lineWidth = constants.textLineWidth;

 context.beginPath();
 // Inner CP
 let textX = xStart;
 let textY = yStart - yPadInner;

 context.beginPath();
 context.fillText(staticCP, textX, textY);
 context.fill();
 }

 function clearStateHots(state) {
 state.hots = [];
 }

 /!**
 * Function draws complement on lines that needs it.
 * @param context context to draw complement to
 * @param line line which have starting point for complement
 * @param switchIO set this to 0 if line is OUTPUT line, or 1 if it is INPUT line
 * @param constants used constants
 *!/
 function drawComplementGeneric(context, line, switchIO, constants) {
 let sign = (switchIO === 0) ? 1 : -1;
 let complement = {
 centerX: line.startX + (sign * constants.IEEEComplementRadius),
 centerY: line.startY,
 radius: constants.IEEEComplementRadius,
 startAngle: toRadians(0),
 endAngle: toRadians(360),
 color: constants.complementColor,
 fillStyle: constants.complementFill,
 lineWidth: constants.complementLineWidth
 };
 drawCircle(context, complement);
 }


 function drawText(context, args, text, centered = true, toggleFill = true) {
 let x1 = (args.leftVertexX === undefined) ? args.startX : args.leftVertexX;
 let y1 = (args.leftVertexY === undefined) ? args.startY : args.leftVertexY;
 let w = args.boxWidth;
 let h = args.boxHeight;

 // Clear canvas
 //context.clearRect(x1, y1, w, h);

 // Set text in the center
 let textX = (centered === true) ? x1 + (w / 2) : x1;
 let textY = (centered === true) ? y1 + (h / 2) : y1;
 context.beginPath();
 context.font = args.textFont;
 context.textAlign = (centered === true) ? "center" : "left";
 context.textBaseline = "middle";
 context.strokeStyle = args.color;
 context.fillStyle = args.fillStyle;

 context.lineWidth = args.lineWidth;
 if (toggleFill === true) {
 context.strokeText(text, textX, textY);
 context.stroke();
 } else {
 context.fillText(text, textX, textY);
 context.stroke();
 }
 }

 function drawClickBox(context, clickBox, state, constants) {
 // Draw box for given linef
 let w = clickBox.clickBoxWidth;
 let h = clickBox.clickBoxHeight;
 let x1 = clickBox.startX;
 let y1 = clickBox.startY;

 context.beginPath();
 context.rect(x1, y1, w, h);
 context.fillStyle = constants.commonFillStyle;
 context.fill();

 saveClickHots(state, x1, y1, w, h, clickBox.clickId);

 context.font = constants.flipFontSize + constants.flipFont;
 context.textAlign = "center";
 context.textBaseline = "middle";
 context.fillStyle = constants.colors.black;
 context.strokeStyle = constants.passiveStateColor;
 context.lineWidth = constants.textLineWidth;

 // Set text in the center
 let textX = x1 + (w / 2);
 let textY = y1 + (h / 2);

 context.strokeText(clickBox.clickText, textX, textY);
 context.stroke();
 }

 function drawClickBoxesNew(context, args, line, constants) {
 // Draw box for given line
 let w = args.clickBoxWidth;
 let h = args.clickBoxHeight;
 let x1 = line.endX - w;
 let y1 = line.startY - (h / 2);

 context.beginPath();
 context.rect(x1, y1, w, h);
 context.fillStyle = constants.commonFillStyle;
 context.fill();

 saveClickHots(args.state, x1, y1, w, h, line.clickId);

 context.font = constants.flipFontSize + constants.flipFont;
 context.textAlign = "center";
 context.textBaseline = "middle";
 context.fillStyle = constants.colors.black;
 context.strokeStyle = constants.passiveStateColor;
 context.lineWidth = constants.textLineWidth;

 // Set text in the center
 let textX = x1 + (w / 2);
 let textY = y1 + (h / 2);

 context.strokeText(args.state.userInput[line.clickId], textX, textY);
 context.stroke();
 }

 function getBoxText(args, o, pos) {
 // Position values: 0 - left, 1 - right, 2 up, 3 down
 let bt = {};
 switch (pos) {
 case 0:
 // Box is drawed left
 bt.startX = args.startX - o.addVal;
 bt.startY = args.startY - o.textLength / 2;
 break;
 case 1:
 // Box is drawed right
 bt.startX = args.startX - o.textLength + o.addVal;
 bt.startY = args.startY - o.textLength / 2;
 break;
 case 2:
 // Box is drawed at the top
 bt.startX = args.startX - o.textLength / 2;
 bt.startY = args.startY - o.addVal;
 break;
 case 3:
 // Box is drawed at bottom
 bt.startX = args.startX - o.textLength / 2;
 bt.startY = args.startY - o.textLength + o.addVal;
 break;
 default:
 // Wrong position
 break;
 }
 return bt;
 }

 function getValueBox(args, o, pos, constants) {
 // Position values: 0 - left, 1 - right, 2 up, 3 down
 let vb = {};

 let xOff = 3 * constants.scaleFactor;
 let yOff = 1 * constants.scaleFactor;
 let yOff2 = 5 * constants.scaleFactor;

 switch (pos) {
 case 0:
 // Box is drawed left
 vb.startX = args.startX - o.textLength - xOff;
 vb.startY = args.startY - o.textLength - yOff2;
 break;
 case 1:
 // Box is drawed right
 vb.startX = args.startX + xOff;
 vb.startY = args.startY - o.textLength - yOff2;
 break;
 case 2:
 // Box is drawed at the top
 vb.startX = args.startX + xOff;
 vb.startY = args.startY - o.textLength - yOff2;
 break;
 case 3:
 // Box is drawed at bottom
 vb.startX = args.startX + xOff;
 vb.startY = args.startY + yOff;
 break;
 default:
 // Wrong position
 break;
 }
 return vb;
 }


 function getCPFix(pos) {
 let offset;

 switch (pos) {
 case 0:
 offset = -5;
 break;
 case 1:
 offset = 5;
 break;
 default:
 offset = 0;
 break;
 }
 return offset;
 }


 function getCPComplementFix(pos, constants) {
 let offset = 0;
 switch (pos) {
 case 0:
 offset -= constants.schemaComplementRadius * 2;
 break;
 case 1:
 offset += constants.schemaComplementRadius * 2;
 break;
 case 2:
 offset -= constants.schemaComplementRadius * 2;
 break;
 case 3:
 offset += constants.schemaComplementRadius * 2;
 break;
 }
 return offset
 }

 /!**
 * Draws text and values for given box
 * @param context context at which box is drawn
 * @param box object representing box
 * @param io set this to 0 if input, otherwise 1 (for output)
 * @param index position in list
 * @param line box line
 * @param pos position of text and values for this box
 * @param constants used constants
 *!/
 function drawBoxTextAndValues(context, box, io, index, line, pos, constants) {
 let outside = 0;
 if (box.textPos != undefined && box.textPos[io][index] === 0) {
 outside = 1;
 }

 let addVal = 0;
 if (outside === 1) {
 addVal += (constants.inputLineLengthNew +
 constants.flipTextBoxWidth);
 if (box.addClickBox[io][index] === true) {
 addVal += box.clickBoxWidth;
 }
 }

 let other = {
 clickBoxLength: box.clickBoxWidth,
 lineLength: constants.inputLineLengthNew,
 textLength: constants.flipTextBoxWidth,
 addVal: addVal
 };
 let typeSpec = 1;
 if (box.text[io][index] === "CP") {
 other.addVal += getCPFix(outside);
 typeSpec = (pos === 0 && box.textPos[io][index] === 1) ? box.flipFlopType : 1;
 } else if (box.text[io][index].length > 2) {
 other.addVal += getCPFix(outside);
 }

 let flipText = getBoxText(line, other, pos);
 flipText.color = line.color;
 flipText.boxWidth = constants.flipTextBoxWidth;
 flipText.boxHeight = constants.flipTextBoxHeight;
 flipText.text = box.text[io][index];

 let valueO = {
 textLength: constants.valuesBoxWidth,
 };

 let lineVal = getValueBox(line, valueO, pos, constants);
 passiveStyle(context, constants);
 if (box.addValues[io][index] === 1) {
 drawValues(context, lineVal, box.lineColors[io][index], constants);
 }

 let text = flipText.text;
 if (text.search("'") != -1) {
 let textOnly = text.replace("'", "");
 drawOverlineText2(context, flipText, textOnly,
 constants.overLineLength, constants);
 } else {
 if (pos === 3 && text === "CP" && box.state.impl != 1) {
 drawCPBelow(context, box, constants, typeSpec);
 }
 drawFlipText(context, flipText, constants, true, false, typeSpec, pos);
 }
 }

 function drawComplexLine(context, line, cond, state,
 constants, direction = 0) {
 passiveStyle(context, constants);
 drawConnectLine(context, line, constants.tubeLineWidth, direction);
 if (cond === constants.greenLineActiveColor) {
 activeStyle(context, state);
 line.color = constants.greenLineActiveColor;
 drawConnectLine(context, line, constants.currentLineWidth, direction);
 }
 passiveStyle(context, constants);
 }

 /!**
 *
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param c initial line color, default is "black"
 * @returns {{startX: *, startY: *, endX: *, endY: *, color: string}}
 *!/
 function getLineParams(x1, y1, x2, y2, c = "black") {
 return {
 startX: x1,
 startY: y1,
 endX: x2,
 endY: y2,
 color: c
 };
 }

 /!**
 *
 * @param args
 * @param o line position parameter
 * @param pos  position values: 0 - left, 1 - right, 2 up, 3 down
 * default = 0
 *!/
 function getBoxLine(args, o, pos = 0) {
 // Position values: 0 - left, 1 - right, 2 up, 3 down
 let line = {};
 switch (pos) {
 case 0:
 // Line is drawed left
 line = getLineParams(
 args.startX + o.addVal,
 args.startY + o.offset,
 args.startX + o.addVal - (o.lineLength),
 args.startY + o.offset
 );

 break;
 case 1:
 // Line is drawed right
 line = getLineParams(
 args.startX + args.boxWidth + o.addVal,
 args.startY + o.offset,
 args.startX + args.boxWidth + (o.lineLength) + o.addVal,
 args.startY + o.offset
 );

 break;
 case 2:
 // Line is drawed at the top
 line = getLineParams(
 args.startX + o.offset,
 args.startY + o.addVal,
 args.startX + o.offset,
 args.startY + o.addVal - o.lineLength
 );
 break;
 case 3:
 // Line is drawed at bottom
 line = getLineParams(
 args.startX + o.offset,
 args.startY + args.boxHeight + o.addVal,
 args.startX + o.offset,
 args.startY + args.boxHeight + o.lineLength + o.addVal
 );
 break;
 default:
 // Wrong position
 break;
 }
 return line;
 }

 function getClickBox(args, o, pos = 0) {
 // Position values: 0 - left, 1 - right, 2 up, 3 down
 let cb = {};
 switch (pos) {
 case 0:
 // Box is drawed left
 cb.startX = args.endX - o.clickBoxWidth;
 cb.startY = args.startY - o.clickBoxHeight / 2;
 break;
 case 1:
 // Box is drawed right
 cb.startX = args.endX;
 cb.startY = args.startY - o.clickBoxHeight / 2;
 break;
 case 2:
 // Box is drawed at the top
 cb.startX = args.startX - o.clickBoxWidth / 2;
 cb.startY = args.endY - o.clickBoxHeight;
 break;
 case 3:
 // Box is drawed at bottom
 cb.startX = args.startX - o.clickBoxWidth / 2;
 cb.startY = args.endY;
 break;
 default:
 // Wrong position
 break;
 }
 return cb;
 }

 /!*function getFixedLine(line, box, constants){
 let l = {};
 switch(box.flipFlopType){
 case 0:
 line.startX -=
 case 1:
 return line;
 break;

 case 2:

 }
 }*!/


 function drawFlipLines(context, args, index, pos, constants) {
 // Draw flip lines.
 let availableLength = 0;
 if (pos === 0 || pos === 1) {
 availableLength = args.boxHeight;
 } else if (pos === 2 || pos === 3) {
 availableLength = args.boxWidth;
 }
 let items = args.text[index].length;
 let lineDistance = availableLength / items;
 let offFromSides = (availableLength - (items - 1) *
 lineDistance) / 2;
 let lineLength = args.lineLength;
 for (let c = 0; c < items; c++) {
 let addVal = 0;
 if (args.text[index][c] === "CP") {
 if (args.flipFlopType === 0 || args.flipFlopType === 2) {
 addVal = getCPComplementFix(pos, constants);
 }
 }
 let other = {
 offFromSides: offFromSides,
 lineLength: lineLength,
 lineDistance: lineDistance,
 c: c,
 offset: offFromSides + c * lineDistance,
 addVal: addVal,
 color: constants.passiveStateColor
 };
 let curLine = getBoxLine(args, other, pos);

 curLine.color = constants.passiveStateColor;
 curLine.text = args.text[index][c];

 if (args.addLines[index][c] === 1) {
 drawComplexLine(context, curLine,
 args.lineColors[index][c], args.state, constants,
 args.lineDirection[pos]);
 }

 drawBoxTextAndValues(context, args, index, c, curLine, pos, constants);
 if (args.addClickBox[index][c] === true) {
 let o = {
 clickBoxWidth: args.clickBoxWidth,
 clickBoxHeight: args.clickBoxHeight
 };
 let clickBox = getClickBox(curLine, o, pos);
 clickBox.clickBoxWidth = o.clickBoxWidth;
 clickBox.clickBoxHeight = o.clickBoxHeight;
 clickBox.clickId = args.clickIds[index][c];
 clickBox.clickText = args.clickText[index][c];
 drawClickBox(context, clickBox, args.state, constants);
 }
 }
 }


 /!**
 * Draws flip flop text for general flip flop box
 * @param context context in which text is drawn
 * @param line line from which starting points are used
 * @param switchIO defines left or right offset depending on line specification
 * @param centered true specifies centered text (default), otherwise text is not centered
 * @param toggleFill true if fill is needed, otherwise stroke is used
 * @param typeSpec specifies FL type: 0 - Low level active, 2 - Falling edge active
 * 3 - Raising edge active, otherwise High logical level active
 * @param constants used constants
 *!/
 function drawFlipFlopText(context, line, switchIO, constants, centered = true,
 toggleFill = true, typeSpec = 0) {
 let sign = (switchIO === 1) ? -constants.textBoxWidth : 0;
 let w = constants.textBoxWidth;
 let h = constants.textBoxHeight;
 let x1 = line.startX + sign;
 let y1 = line.startY - (h / 2);

 // Set text in the center
 let textX = (centered === true) ? x1 + (w / 2) : x1;
 let textY = (centered === true) ? y1 + (h / 2) : y1;

 // Type specification of flip flop, can be logical level or edge behaviour
 let xPadInner = 8 * constants.scaleFactor;
 if (typeSpec === 0) {
 drawComplementIndicator(context, x1, line.startY, constants);
 textX += constants.schemaComplementRadius * 2;
 xPadInner += constants.schemaComplementRadius * 2;
 textX += xPadInner;
 } else if (typeSpec === 2) {
 drawComplementIndicator(context, x1, line.startY, constants);
 drawEdgeIndicator(context, x1, textY, constants, 1);
 textX += constants.schemaComplementRadius * 2;
 xPadInner += constants.schemaComplementRadius * 2;
 textX += xPadInner;
 } else if (typeSpec === 3) {
 drawEdgeIndicator(context, x1, line.startY, constants, 1);
 textX += xPadInner;
 }

 context.beginPath();
 context.font = constants.registerFontSize + constants.registerFont;// constants.flipFontSizeInner + constants.flipFont;
 context.textAlign = (centered === true) ? "center" : "left";
 context.textBaseline = "middle";
 context.fillStyle = constants.colors.black;
 context.strokeStyle = constants.colors.black;
 context.lineWidth = constants.textLineWidth;
 if (toggleFill === true) {
 context.strokeText(line.text, textX, textY);
 context.stroke();
 } else {
 context.fillText(line.text, textX, textY);
 context.stroke();
 }
 }


 /!**
 * Draws flip flop text for general flip flop box
 * @param context context in which text is drawn
 * @param box line from which starting points are used
 * @param constants used constants
 * @param centered true specifies centered text (default), otherwise text is not centered
 * @param toggleFill true if fill is needed, otherwise stroke is used
 * @param typeSpec specifies FL type: 0 - Low level active, 2 - Falling edge active
 * 3 - Raising edge active, otherwise High logical level active
 * @param pos drawing position
 *!/
 function drawFlipText(context, box, constants, centered = true,
 toggleFill = true, typeSpec = 0, pos = 0) {
 let w = box.boxWidth;
 let h = box.boxHeight;
 let x1 = box.startX;
 let y1 = box.startY;

 // Set text in the center
 let textX = (centered === true) ? x1 + (w / 2) : x1;
 let textY = (centered === true) ? y1 + (h / 2) : y1;


 let centerX = x1 - 5;
 let centerY = y1 + box.boxHeight / 2;
 // Type specification of flip flop, can be logical level or edge behaviour
 let xPadInner = 8 * constants.scaleFactor;
 if (pos != 3) {
 if (typeSpec === 0) {
 let x1 = centerX + constants.schemaComplementRadius;
 let y1 = centerY - constants.schemaComplementRadius;
 drawComplementIndicator(context, x1, y1, constants, constants);
 textX += xPadInner;
 } else if (typeSpec === 2) {
 let x1 = centerX + constants.schemaComplementRadius;
 let y1 = centerY - constants.schemaComplementRadius;
 drawComplementIndicator(context, x1, y1, constants, constants);
 textX += xPadInner;
 drawEdgeIndicator(context, x1 + constants.schemaComplementRadius, centerY, constants, 1);
 textX += xPadInner;
 } else if (typeSpec === 3) {
 drawEdgeIndicator(context, centerX, centerY, constants, 1);
 textX += xPadInner;
 }
 }

 context.beginPath();
 context.font = constants.registerFontSize + constants.registerFont;
 context.textAlign = (centered === true) ? "center" : "left";
 context.textBaseline = "middle";
 context.fillStyle = constants.colors.black;
 context.strokeStyle = constants.colors.black;
 context.lineWidth = constants.textLineWidth;
 if (toggleFill === true) {
 context.strokeText(box.text, textX, textY);
 context.stroke();
 } else {
 context.fillText(box.text, textX, textY);
 context.stroke();
 }
 }


 function getBinaryString(array) {
 let str = "";
 for (let c = 0, l = array.length; c < l; c++) {
 str += array[c].toString();
 }
 return str;
 }

 function drawDecodedState(context, regBox, state, constants) {
 let boxHeight = constants.decoderBoxHeight;
 let fillStyle = constants.colors.decoderColor;

 // Determine middle position

 let yOff = 10;
 let charArray = state.Q.map(String);
 let binaryString = getBinaryString(charArray);

 let decoder = {
 startY: yOff,
 boxHeight: boxHeight,
 fillStyle: fillStyle,
 color: constants.colors.decoderColor,
 stateDecimal: binaryToDecimal(binaryString),
 stateBinary: binaryString,
 lineWidth: constants.textLineWidth
 };

 let letterWidth = 2 * constants.scaleFactor;
 let equals = " = ";
 let numOfLetters = 4 * decoder.stateBinary.length + equals.length;

 let boxWidth = constants.decoderBoxWidth + (numOfLetters * letterWidth);
 let xOff = regBox.startX + regBox.boxWidth / 2 - boxWidth / 2;
 decoder.boxWidth = boxWidth;
 decoder.startX = xOff;


 let decoderText = {
 startX: decoder.startX,
 startY: decoder.startY,
 boxWidth: decoder.boxWidth,
 boxHeight: decoder.boxHeight,
 fillStyle: constants.colors.decoderTextColor,
 stateDecimal: binaryToDecimal(binaryString),
 stateBinary: binaryString,
 color: constants.colors.decoderTextColor,
 textFont: constants.decoderFontSize + constants.registerFont,
 lineWidth: constants.textLineWidth
 };

 let index2 = 8322;
 let index1 = 8321;
 let index0 = 8320;
 let stringToPrint = decoderText.stateBinary + String.fromCharCode(index2)
 + equals + decoderText.stateDecimal + String.fromCharCode(index1)
 + String.fromCharCode(index0);

 drawFlipFlopBox(context, decoder, constants);
 drawText(context, decoderText, stringToPrint, true, false);
 }

 */
