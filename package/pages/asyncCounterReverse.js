/**
 * Created by Komediruzecki on 16.1.2017..
 */
/**
 * Created by Komediruzecki on 7.1.2017..
 */


FasyncCounterReverse();

function FasyncCounterReverse() {
    let canvas = document.getElementById("asyncCounterReverse");
    let context = canvas.getContext("2d");

    let currentSpeed = 50;
    let numOfFlips = 4;
    let scaleFactor = 1.6;
    let constReg = new Constants(scaleFactor);
    let stateReg = {
        simName: "Asinkrono binarno brojilo unatrag",
        // D0, D1, D2, D3, CLK, IMPL-SWITCH
        T: getArray(0, numOfFlips),
        Q: getArray(0, numOfFlips),
        nQ: getArray(1, numOfFlips),
        CP: 0,
        // sIn: 0,
        edge: 0,
        hots: [],
        impl: 1,
        animationOffset: 0,
        animationPattern: [14, 6],
        scaleFactor: 1.8,
        flipsCount: {
            min: 1,
            max: 20,
            minInner: 1,
            maxInner: 8,
            resetTo: 4,
            numOfFlips: numOfFlips
        },
        msDelay: 0,
        disableCP: 0,
        countDelay: 0,
        calculatingT: getArray(0, numOfFlips)
    };
    stateReg.cpId = 0;
    stateReg.implId = 2;
    stateReg.addFlipId = 3;
    stateReg.addDelayId = 4;

    let stateT = {
        simName: "Implementacija asinkronog binarnog brojila unatrag",
        T: stateReg.D,
        CP: stateReg.CP,
        CPEdge: stateReg.CPEdge,
        Q: stateReg.Q,
        nQ: stateReg.nQ,
        userInput: stateReg.userInput,
        animationOffset: 0,
        animationPattern: [11, 6],
        impl: 1
    };


    Object.defineProperties(stateT, {
        "setQ": {
            set: function (newVal) {
                stateT.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateT.nQ = newVal;
            }
        },
    });

    let min = 1.4;
    let max = 1.6;
    let scales = fillScales(min, max);
    let zoomHotsReg = {
        min: min,
        max: max,
        minReset: min,
        maxReset: max,
        blackBoxMin: getArray(min, 20),//.concat(scales.blackBoxMin),
        blackBoxMax: getArray(max, 15).concat(scales.blackBoxMax),
        innerMin: getArray(min, 6).concat(scales.innerImplMin),
        innerMax: getArray(max, 4).concat(scales.innerImplMax),
        standard: stateReg.scaleFactor,
        hots: []
    };


    saveClickHots(zoomHotsReg, 0, 0, canvas.width, canvas.height);


    function fillScales(min, max) {
        let scales = {};
        scales.blackBoxMax = fillArray(max, -0.02, 5);
        // scales.blackBoxMin = fillArray(min, -0.05, 5);
        scales.innerImplMax = fillArray(max, -0.1, 4);
        scales.innerImplMin = fillArray(min, -0.1, 2);
        return scales;
    }

    function setProperScales(numOfFlips, impl) {
        let newScaleFactor;
        switch (impl) {
            case 0:
                zoomHotsReg.min = zoomHotsReg.blackBoxMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.blackBoxMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            case 1:
                zoomHotsReg.min = zoomHotsReg.innerMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.innerMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            default:
                newScaleFactor = stateReg.scaleFactor;
        }
        setScaleFactor(newScaleFactor);
    }

    function setDefaultScales() {
        zoomHotsReg.max = zoomHotsReg.maxReset;
        zoomHotsReg.min = zoomHotsReg.minReset;
        let newScaleFactor = zoomHotsReg.standard;
        setScaleFactor(newScaleFactor);
    }

    function setScaleFactor(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function processZoom(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function startSimulation() {
        stateReg.animationOffset++;
        if (stateReg.animationOffset > 2000) {
            stateReg.animationOffset = 0;
        }
        stateT.animationOffset++;
        if (stateT.animationOffset > 2000) {
            stateT.animationOffset = 0;
        }
        update();
    }

    function updateState(state, newNumOfFlips) {
        let T = state.T;
        let Q = state.Q;
        let nQ = state.nQ;
        let calcT = state.calculatingT;
        let oldNumOfFlips = state.flipsCount.numOfFlips;
        if (newNumOfFlips > oldNumOfFlips) {
            T = T.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            Q = Q.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            nQ = nQ.concat(getArray(1, newNumOfFlips - oldNumOfFlips));
            calcT = calcT.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
        } else if (newNumOfFlips < oldNumOfFlips) {
            T = T.slice(0, newNumOfFlips);
            Q = Q.slice(0, newNumOfFlips);
            nQ = nQ.slice(0, newNumOfFlips);
            calcT = calcT.slice(0, newNumOfFlips);
        } else {
            // No changes
        }

        state.flipsCount.numOfFlips = newNumOfFlips;
        let newState = {
            simName: state.simName,
            // D0, D1, D2, D3, CLK, IMPL-SWITCH
            T: T,
            Q: Q,
            nQ: nQ,
            CP: state.CP,
            edge: state.edge,
            hots: state.hots,
            impl: state.impl,
            animationOffset: state.animationOffset,
            animationPattern: state.animationPattern,
            scaleFactor: state.scaleFactor,
            flipsCount: state.flipsCount,
            msDelay: state.msDelay,
            disableCP: state.disableCP,
            countDelay: state.countDelay,
            calculatingT: calcT
        };
        newState.cpId = 0;
        newState.implId = 2;
        newState.addFlipId = 3;
        newState.addDelayId = 4;
        return newState;
    }

    function checkFlipsCount(state, oldNumOfFlips) {
        let impl = state.impl;
        let reset = false;
        let newNumOfFlips = oldNumOfFlips;
        switch (impl) {
            case 0:
                if (newNumOfFlips < state.flipsCount.min) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;

                }
                if (newNumOfFlips > state.flipsCount.max) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
            case 1:
                if (newNumOfFlips < state.flipsCount.minInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                if (newNumOfFlips > state.flipsCount.maxInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
        }
        if (reset) {
            setDefaultScales();
        }
        setProperScales(newNumOfFlips, impl);
        return newNumOfFlips;
    }


    function clickHotEvent2(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };
        let noClick = true;
        // Check if click was in area which we consider
        for (let i = 0, l = stateReg.hots.length; i < l; i++) {
            let h = stateReg.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
                // We are inside click hot
                // TO-DO logic for user clicks
                switch (h.row) {
                    case (stateReg.cpId):
                        // CP changed
                        if ((stateReg.disableCP === 0)) {
                            animateFallingCP(stateReg, h.row);
                        }
                        break;
                    case stateReg.addFlipId:
                        if (stateReg.disableCP === 0) {
                            let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                            let newNumOfFlips = calculateFlips(e, oldNumOfFlips);
                            let checkedFlipsCount = checkFlipsCount(stateReg,
                                newNumOfFlips);
                            stateReg = updateState(stateReg, checkedFlipsCount);
                        }

                        break;
                    case stateReg.implId:
                        let foundRightClick = false;
                        if ("which" in e) {
                            if (e.which === 3) {
                                foundRightClick = true;
                            }
                        } else if (e.button === 2) {
                            foundRightClick = true;
                        }
                        if (foundRightClick) {
                            propRightClick(e, stateReg, h);
                            let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                            let checkedFlipsCount = checkFlipsCount(stateReg,
                                oldNumOfFlips);
                            stateReg = updateState(stateReg, checkedFlipsCount);
                        }
                        break;
                    case stateReg.addDelayId:
                        if (stateReg.disableCP === 0) {
                            stateReg.countDelay++;
                            let delay = 500;
                            let factor = stateReg.countDelay;
                            if (factor === 5) {
                                stateReg.countDelay = 0;
                                factor = 0;
                            }
                            stateReg.msDelay = delay * factor;
                        }
                        break;
                    default:
                        alert("Impossible click!");
                }
                update();
                noClick = false;
                break;
            }
        }
    }

    function getArray(value, numOfElements) {
        let array = [];
        for (let i = numOfElements - 1; i >= 0; i--) {
            array.push(value);
        }
        return array;
    }

    function drawRegImpl(canvas, context, outerState, stateT, constants) {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateT.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateT);
        clearStateHots(stateReg);

        // Init colors
        let colorsOut = getColorStyle(stateReg.Q, constants);
        let colorsLeft = getColorStyle([0, 0, stateReg.CP], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        // Inner D flip flops
        let numOfFlips = outerState.flipsCount.numOfFlips;
        let scaleT = constants.scaleFactor - 0.5;
        let constT = new Constants(scaleT);
        let flipDWidth = 50 * constT.scaleFactor;
        let flipDHeight = 67 * constT.scaleFactor;
        let dist = 60 * constT.scaleFactor;
        let holes = numOfFlips - 1;
        // Out box for holding D flip flops

        let boxWidth = dist * (holes + 1) + 2 * constants.inputLineLengthD +
            flipDWidth * numOfFlips * (constants.scaleFactor - 0.5);
        let boxHeight = 110 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        if (numOfFlips === 8) {
            xOff += 35;
        }

        let yOff = -10 * constants.scaleFactor;
        let regBox = {
            startX: (canvas.width - boxWidth) / 2,
            startY: (canvas.height - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth + xOff,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            boxLineWidth: constants.boxLineWidth,
            lineColors: [colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            strokeWidth: constants.boxStrokeWidth,
            state: stateReg,
            flipFlopType: 1,
            // Indexes are: 0, 1, 2, 3:
            // 0 and 1 for left and right, 2 and 3 for up and down
            // 0 - Draw from left to right and from up to down, 1 inverse of 0.
            lineDirection: [0, 0, 1, 1],
            clickText: [
                getArray(-1, numOfFlips),
                [-1, -1, outerState.CP],
            ]
        };

        let textLeft = ["", "", "CP"];
        // Define Box Elements
        regBox.addLines = [
            getArray(1, numOfFlips),
        ];
        regBox.addValues = [
            getArray(1, numOfFlips),
        ];
        regBox.addClickBox = [
            getArray(false, numOfFlips),
        ];

        regBox.clickIds = [
            getArray(-1, numOfFlips),
        ];

        // Init text
        let letterOut = "Q";
        let textOut = [];
        let unicodeSubScript0 = 8320;
        for (let c = 0; c < numOfFlips; c++) {
            // Make this generic...
            if (c < 10) {
                textOut.push(letterOut + String.fromCharCode(unicodeSubScript0 + c));
            } else if (c < 20) {
                let uni1 = 8321;
                let uni2 = unicodeSubScript0 + c - 10;
                textOut.push(letterOut +
                    String.fromCharCode(uni1) + String.fromCharCode(uni2));
            }
        }

        regBox.text = [
            textOut,
            textLeft
        ];

        regBox.textPos = [
            getArray(0, numOfFlips),
        ];

        // addBoxElements(regBox, numOfFlips);
        let CPClickBox = [false, false, true];
        let CPValues = [0, 0, 1];
        let CPLines = [0, 0, 1];
        let CPTextPos = [0, 0, 0];
        let CPClickIds = [-1, -1, outerState.cpId];
        regBox.addValues.push(CPValues);
        regBox.addLines.push(CPLines);
        regBox.textPos.push(CPTextPos);
        regBox.addClickBox.push(CPClickBox);
        regBox.clickIds.push(CPClickIds);

        // Save click hots for view change
        let regBoxClickId = outerState.implId;
        saveClickHots(outerState, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let leftPosition = 0;
        let outputPosition = 2;
        let leftIndex = 1;
        let outLineIndex = 0;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        // Draw Inner Clock
        drawFlipLines(context, regBox, leftIndex, leftPosition, constants);

        // Draw D Flip Flops
        let jumpAhead = (regBox.boxWidth -
            (numOfFlips * flipDWidth + holes * dist)) / 2;
        let yOffInner = (regBox.boxHeight - (flipDHeight)) / 2;
        let flipDDistance = dist;
        dist += flipDWidth;

        // Draw inner D flip flops
        // Draw CP Line
        //drawCPLine(context, regBox, constants);

        for (let c = numOfFlips - 1; c >= 0; c--) {
            let curD = {
                xCoord: regBox.startX + jumpAhead + (dist * c),
                yCoord: regBox.startY + (yOffInner),
                state: stateT,
                boxWidth: flipDWidth,
                boxHeight: flipDHeight
            };
            drawT(c, context, regBox, curD, constT, flipDDistance);
        }

        // Draw decoder state
        drawDecodedState(context, regBox, regBox.state, constants);
    }


    function update() {
        if (stateReg.impl === 0) {
            drawReg(canvas, context, stateReg, constReg);
        } else {
            drawRegImpl(canvas, context, stateReg, stateT, constReg);
        }
    }

    function getColorStyle(listToCheck, constants) {
        let listLength = listToCheck.length;
        let colorsStlyes = [];
        for (let c = 0; c < listLength; c++) {
            if (listToCheck[c] === 1) {
                colorsStlyes.push(constants.greenLineActiveColor);
            } else if (listToCheck[c] === 0) {
                colorsStlyes.push(constants.passiveStateColor);
            }
        }
        return colorsStlyes;
    }

    function addClickAdder(context, box, constants) {
        let x = 731;
        let y = 380;
        let x2 = 170;
        let clickToAddDelay = {
            startX: x,
            startY: y,
            clickBoxWidth: 2 * constants.textBoxWidth,
            clickBoxHeight: 2 * constants.textBoxHeight,
            clickId: box.state.addDelayId,
            clickText: box.state.countDelay * 500 + " ms"
        };
        let clickToAddFlip = {
            startX: x2,
            startY: y,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            clickId: box.state.addFlipId,
            clickText: box.state.flipsCount.numOfFlips
        };
        drawClickBox(context, clickToAddFlip, box.state, constants);
        drawClickBox(context, clickToAddDelay, box.state, constants);
    }

    function drawReg(canvas, context, state, constants) {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateReg);

        // Init colors
        //let colorsIn = getColorStyle([state.CP], constants);
        let colorsOut = getColorStyle(state.Q, constants);
        let colorsLeft = getColorStyle([state.CP], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        let numOfFlips = state.flipsCount.numOfFlips;
        let boxWidth = 40 + (numOfFlips * constants.clickBoxWidth) * constants.scaleFactor;
        let boxHeight = 60 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;

        let regBox = {
            startX: (canvas.width - boxWidth) / 2 + xOff,
            startY: (canvas.height - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            flipFlopType: 2,
            lineDirection: [0, 0, 1, 1],
            clickText: [
                getArray(-1, numOfFlips),
                [state.CP]
            ],
            lineColors: [colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            lineWidth: constants.boxStrokeWidth,
            state: state
        };
        let textLeft = ["CP"];

        // Define Box Elements
        regBox.addLines = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addValues = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addClickBox = [
            getArray(false, numOfFlips),
            getArray(true, 1),
        ];

        regBox.clickIds = [
            getArray(-1, numOfFlips),
            [state.cpId],
        ];

        // Init text
        let letterOut = "Q";
        let textOut = [];
        let unicodeSubScript0 = 8320;
        for (let c = 0; c < numOfFlips; c++) {
            // Make this generic...
            if (c < 10) {
                textOut.push(letterOut + String.fromCharCode(unicodeSubScript0 + c));
            } else if (c < 20) {
                let uni1 = 8321;
                let uni2 = unicodeSubScript0 + c - 10;
                textOut.push(letterOut +
                    String.fromCharCode(uni1) + String.fromCharCode(uni2));
            }
        }

        regBox.text = [
            textOut,
            textLeft
        ];

        regBox.textPos = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];

        // Save click hots for view change
        let regBoxClickId = state.implId;
        saveClickHots(state, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let outputPosition = 2;
        let clkPosition = 0;
        let outLineIndex = 0;
        let leftLineIndex = 1;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        drawFlipLines(context, regBox, leftLineIndex,
            clkPosition, constants);

        // Additional items

        // Draw decoder state
        drawDecodedState(context, regBox, regBox.state, constants);
    }

    function wheelEventHandling(e) {
        myMouseWheelEvent(e, canvas, zoomHotsReg);
        update();
    }

    function myMouseWheelEvent(e, canvas, hots) {
        e.preventDefault();
        mouseZoomer(e, canvas, hots, processZoom, stateReg.scaleFactor);
    }

    function propRightClick(e, state, h) {
        if (h.row === state.implId) {
            state.impl = (state.impl === 1) ? 0 : 1;
            setScaleFactor(state.scaleFactor);
            e.preventDefault();
        }
    }

    function rightClickHot(e) {
        e.preventDefault();
    }

    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEvent2);
        canvas.addEventListener('mousewheel', wheelEventHandling, false);
        canvas.addEventListener('contextmenu', rightClickHot, false);

        // Remove dblclk on canvas to select text in document.
        canvas.addEventListener('selectstart',
            function (e) {
                e.preventDefault();
                return false;
            }, false);
        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', wheelEventHandling, false)

    }

    function init(canvas, context, state, constants) {
        if (state.impl === 0) {
            drawReg(canvas, context, state, constants);
        } else {
            drawRegImpl(canvas, context, stateReg, stateT, constReg);
        }
    }

    function getYLinesInnerOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }

    function getYLinesOutOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }


    function drawQToDLine(ID, context, outBox, args, constants) {
        let colorQ = getColorStyle([args.state.Q[ID]], constants);
        let colorNQM1 = getColorStyle([args.state.nQ[ID - 1]], constants);

        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let yOff = 35 * constants.scaleFactor;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff - yOff,
            constants.passiveStateColor
        );

        let outQLeft1 = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[0].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[0].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.passiveStateColor
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.passiveStateColor
        );

        //let yOffCP = getYLinesOutOff(outBox.boxHeight, outBox.text[1].length);
        let addVal = getCPComplementFix(2, constants);

        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft1, colorQ[0], args.state, constants, 0);

        let inNQtoCP = getLineParams(
            args.startX - args.flipDDistance + args.lineLength,
            args.startY + args.boxHeight - yLinesOff,
            args.startX - args.lineLength + addVal,
            args.startY + args.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        drawComplexLine(context, inNQtoCP, colorNQM1[0], args.state, constants, 0);

    }

    function drawSinLine(ID, context, outBox, args, constants) {
        let colorQ = getColorStyle([args.state.Q[ID]], constants);
        let colorCP = getColorStyle([args.state.CP], constants);

        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let yOff = 35 * constants.scaleFactor;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff - yOff,
            constants.passiveStateColor
        );

        let outQLeft1 = getLineParams(
            args.startX + args.boxWidth + args.flipDDistance / 2,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            constants.passiveStateColor
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[0].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[0].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.passiveStateColor
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.passiveStateColor
        );


        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft1, colorQ[0], args.state, constants, 0);

        let yLinesOutOffH = getYLinesOutOff(outBox.boxHeight, outBox.text[1].length);
        let inDDown = getLineParams(
            args.startX - args.lineLength - 2 * constants.schemaComplementRadius,
            args.startY + args.boxHeight - yLinesOff,
            //outBox.startY + outBox.boxHeight - yLinesOutOffH,
            args.startX - args.lineLength - 2 * constants.schemaComplementRadius,
            outBox.startY + outBox.boxHeight - yLinesOutOffH,
            constants.passiveStateColor
        );

        let inDLeft = getLineParams(
            inDDown.endX,
            inDDown.endY,
            outBox.startX,
            inDDown.endY,
            constants.passiveStateColor
        );

        drawComplexLine(context, inDDown, colorCP[0], args.state, constants, 1);
        drawComplexLine(context, inDLeft, colorCP[0], args.state, constants, 0);

        /* let dotCP = {
         centerX: outCPLine.startX,
         centerY: outCPLine.startY,
         radius: constants.IEEEConnectDotRadius,
         startAngle: toRadians(constants.zeroAngle),
         endAngle: toRadians(constants.fullCircle),
         rotation: false,
         color: constants.passiveStateColor,
         fillStyle: constants.passiveStateColor,
         lineWidth: constants.connectDotLineWidth
         };
         drawCircle(context, dotCP);*/
    }

    function drawT(ID, context, outBox, args, constants, dist) {

        // Init Colours
        stateT.T = outBox.state.T;
        stateT.CP = outBox.state.CP;
        stateT.Q = outBox.state.Q;
        stateT.nQ = outBox.state.nQ;
        stateT.impl = outBox.state.impl;

        let colorsIn;
        if (ID === 0) {
            colorsIn = getColorStyle([1, stateT.CP], constants);
        } else {
            colorsIn = getColorStyle([1, stateT.nQ[ID - 1]], constants);
        }
        let colorsOut = getColorStyle([stateT.Q[ID], stateT.nQ[ID]], constants);
        let colorsBox = {box: constants.colors.flipT, clickBox: constants.colors.clickBoxColor};

        let boxWidth = args.boxWidth;
        let boxHeight = args.boxHeight;
        let xOff = args.xCoord;
        let yOff = args.yCoord;
        let genericBox = {
            startX: xOff,
            startY: yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthD,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            clickIds: [
                [-1, -1],
                [-1, -1],
            ],
            textPos: [[1, 1], [1, 1]],
            addLines: [[1, 1], [1, 1]],
            lineDirection: [0, 0, 0, 1],
            addValues: [[1, 1], [1, 1]],
            addClickBox: [
                [false, false],
                [false, false]
            ],
            text: [["T", "CP"], ["Q", "Q'"]],
            lineColors: [colorsIn, colorsOut],
            flipFlopType: 2,
            boxLineWidth: constants.boxLineWidth,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: stateT,
            flipDDistance: dist

        };

        // Draw flip flop. Draw IO lines
        passiveStyle(context, constants);
        if ((outBox.state.calculatingT[ID] === 1) && (outBox.state.msDelay != 0)) {
            let glowWidth = 3;
            drawGlowingBox(context, genericBox, glowWidth, constants);
        }

        drawFlipFlopBox(context, genericBox, constants);
        drawFlipLines(context, genericBox, 0, 0, constants);
        drawFlipLines(context, genericBox, 1, 1, constants);
        //   drawCPBelow(context, genericBox, constants, genericBox.flipFlopType);
        if (ID === 0) {
            drawSinLine(ID, context, outBox, genericBox, constants);
        } else {
            drawQToDLine(ID, context, outBox, genericBox, constants);
        }
        passiveStyle(context, constants);
    }

    function animateRecursion(state, row) {
        state.calculatingT[row] = 1;
        setTimeout(function () {
            state.Q[row] = (state.Q[row] === 1) ? 0 : 1;
            state.nQ[row] = (state.Q[row] === 1) ? 0 : 1;
            let next = row + 1;
            state.calculatingT[row] = 0;
            if (state.nQ[next] != undefined) {
                if (state.nQ[row] === 0) {
                    animateRecursion(state, next);
                } else {
                    state.disableCP = 0;
                }
            } else {
                state.disableCP = 0;
            }
        }, state.msDelay);
    }

    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateFallingCP(state, row) {
        state.disableCP = 1;
        state.CP = (state.CP === 1) ? 0 : 1;
        if (state.CP === 0) {
            state.CPEdge = 1;
        } else {
            state.CPEdge = 0;
        }
        if (state.CPEdge === 1) {
            state.calculatingT[row] = 1;
            setTimeout(function () {
                state.calculatingT[row] = 0;
                state.Q[row] = (state.Q[row] === 1) ? 0 : 1;
                state.nQ[row] = (state.Q[row] === 1) ? 0 : 1;
                let next = row + 1;
                if (state.nQ[next] != undefined) {
                    if (state.nQ[row] === 0) {
                        animateRecursion(state, next);
                    } else {
                        stateReg.disableCP = 0;
                    }
                } else {
                    state.disableCP = 0;
                }
            }, state.msDelay);
        } else {
            state.disableCP = 0;
        }
        state.CPEdge = 0;
    }

    init(canvas, context, stateReg, constReg);
    eventListeners(canvas);
    setInterval(startSimulation, currentSpeed);
}