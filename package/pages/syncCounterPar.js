/**
 * Created by Komediruzecki on 18.1.2017..
 */


FsyncCounterPar();

function FsyncCounterPar() {
    let canvas = document.getElementById("syncCounterPar");
    let context = canvas.getContext("2d");

    let currentSpeed = 50;
    let numOfFlips = 4;
    let scaleFactor = 1.5;
    let constReg = new Constants(scaleFactor);
    let stateReg = {
        simName: "Sinkrono binarno brojilo s paralelnim prijenosom",
        // D0, D1, D2, D3, CLK, IMPL-SWITCH
        T: getArray(1, 1).concat(getArray(0, numOfFlips - 1)),
        Q: getArray(0, numOfFlips),
        nQ: getArray(1, numOfFlips),
        ands: getArray(0, numOfFlips - 2),
        CP: 0,
        edge: 0,
        hots: [],
        impl: 1,
        animationOffset: 0,
        animationPattern: [14, 6],
        scaleFactor: scaleFactor,
        flipsCount: {
            min: 1,
            max: 8,
            minInner: 1,
            maxInner: 8,
            resetTo: 4,
            numOfFlips: numOfFlips
        },
        msDelay: 0,
        disableCP: 0,
        countDelay: 0,
        calculatingT: 0,
        calculatingAnd: 0,
        valueChangeQ: getArray(0, numOfFlips),
        cpChanged: 0
    };
    stateReg.cpId = 0;
    stateReg.implId = 2;
    stateReg.addFlipId = 3;
    stateReg.addDelayId = 4;

    let stateT = {
        simName: "Implementacija sinkronog binarnog brojila\ns paralelnim prijenosom",
        T: stateReg.T,
        CP: stateReg.CP,
        CPEdge: stateReg.CPEdge,
        Q: stateReg.Q,
        nQ: stateReg.nQ,
        userInput: stateReg.userInput,
        animationOffset: 0,
        animationPattern: [14, 6],
        impl: 1
    };


    Object.defineProperties(stateT, {
        "setQ": {
            set: function (newVal) {
                stateT.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateT.nQ = newVal;
            }
        },
    });

    let min = 1.3;
    let max = 1.5;
    let scales = fillScales(min, max);
    let zoomHotsReg = {
        min: min,
        max: max,
        minReset: min,
        maxReset: max,
        blackBoxMin: getArray(min, 20),//.concat(scales.blackBoxMin),
        blackBoxMax: getArray(max, 15).concat(scales.blackBoxMax),
        innerMin: getArray(min, 6).concat(scales.innerImplMin),
        innerMax: getArray(max, 4).concat(scales.innerImplMax),
        standard: stateReg.scaleFactor,
        hots: []
    };

    saveClickHots(zoomHotsReg, 0, 0, canvas.width, canvas.height);

    function setScaleFactor(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function processZoom(newScaleFactor) {
        stateReg.scaleFactor = newScaleFactor;
        constReg = new Constants(stateReg.scaleFactor);
    }

    function startSimulation() {
        stateReg.animationOffset++;
        if (stateReg.animationOffset > 2000) {
            stateReg.animationOffset = 0;
        }
        stateT.animationOffset++;
        if (stateT.animationOffset > 2000) {
            stateT.animationOffset = 0;
        }
        update();
    }

    function updateState(state, newNumOfFlips) {
        let T = state.T;
        let Q = state.Q;
        let nQ = state.nQ;
        let ands = state.ands;
        let changedQ = state.valueChangeQ;
        let oldNumOfFlips = state.flipsCount.numOfFlips;
        if (newNumOfFlips > oldNumOfFlips) {
            T = T.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            Q = Q.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            nQ = nQ.concat(getArray(1, newNumOfFlips - oldNumOfFlips));
            changedQ = changedQ.concat(getArray(0, newNumOfFlips - oldNumOfFlips));
            if (oldNumOfFlips >= 2) {
                ands = ands.concat(getArray(0, 1));
                // calculatingAnd = calculatingAnd.concat(getArray(0, 1));
            }
        } else if (newNumOfFlips < oldNumOfFlips) {
            T = T.slice(0, newNumOfFlips);
            Q = Q.slice(0, newNumOfFlips);
            nQ = nQ.slice(0, newNumOfFlips);
            changedQ = changedQ.slice(0, newNumOfFlips);
            if (oldNumOfFlips >= 3) {
                ands = ands.slice(0, newNumOfFlips - 2);
                // calculatingAnd = calculatingAnd.slice(0, newNumOfFlips - 2);
            }
        } else {
            // No changes?
        }

        state.flipsCount.numOfFlips = newNumOfFlips;
        let newState = {
            simName: state.simName,
            // D0, D1, D2, D3, CLK, IMPL-SWITCH
            T: T,
            Q: Q,
            nQ: nQ,
            ands: ands,
            CP: state.CP,
            edge: state.edge,
            hots: state.hots,
            impl: state.impl,
            animationOffset: state.animationOffset,
            animationPattern: state.animationPattern,
            scaleFactor: state.scaleFactor,
            flipsCount: state.flipsCount,
            msDelay: state.msDelay,
            disableCP: state.disableCP,
            countDelay: state.countDelay,
            calculatingT: 0,
            calculatingAnd: 0,
            valueChangeQ: changedQ,
            cpChanged: state.cpChanged
        };
        newState.cpId = 0;
        newState.implId = 2;
        newState.addFlipId = 3;
        newState.addDelayId = 4;
        return newState;
    }


    function fillScales(min, max) {
        let scales = {};
        scales.blackBoxMax = fillArray(max, -0.02, 5);
        //scales.blackBoxMin = fillArray(min, -0.05, 5);
        scales.innerImplMax = fillArray(max, -0.093, 4);
        scales.innerImplMin = fillArray(min, -0.093, 2);
        return scales;
    }

    function setProperScales(numOfFlips, impl) {
        let newScaleFactor;
        switch (impl) {
            case 0:
                zoomHotsReg.min = zoomHotsReg.blackBoxMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.blackBoxMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            case 1:
                zoomHotsReg.min = zoomHotsReg.innerMin[numOfFlips - 1];
                zoomHotsReg.max = zoomHotsReg.innerMax[numOfFlips - 1];
                newScaleFactor = zoomHotsReg.max;
                break;
            default:
                newScaleFactor = stateReg.scaleFactor;
        }
        setScaleFactor(newScaleFactor);
    }

    function calculateFlips(e, oldNumOfFlips) {
        let newNumOfFlips = oldNumOfFlips;
        if ("which" in e) {
            if (e.which === 1) {
                newNumOfFlips++
            } else if (e.which === 3) {
                newNumOfFlips--;
            }
        } else if (e.button === 2) {
            newNumOfFlips--;
        } else if (e.button === 0) {
            newNumOfFlips++;
        }

        return newNumOfFlips;
    }

    function checkFlipsCount(state, oldNumOfFlips) {
        let impl = state.impl;
        let reset = false;
        let newNumOfFlips = oldNumOfFlips;
        switch (impl) {
            case 0:
                if (newNumOfFlips < state.flipsCount.min) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;

                }
                if (newNumOfFlips > state.flipsCount.max) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
            case 1:
                if (newNumOfFlips < state.flipsCount.minInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                if (newNumOfFlips > state.flipsCount.maxInner) {
                    newNumOfFlips = state.flipsCount.resetTo;
                    reset = true;
                }
                break;
        }
        if (reset) {
            setDefaultScales();
        }
        setProperScales(newNumOfFlips, impl);
        return newNumOfFlips;
    }

    function setDefaultScales() {
        zoomHotsReg.max = zoomHotsReg.maxReset;
        zoomHotsReg.min = zoomHotsReg.minReset;
        let newScaleFactor = zoomHotsReg.standard;
        setScaleFactor(newScaleFactor);
    }


    function clickHotEvent2(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };
        // Check if click was in area which we consider
        for (let i = 0, l = stateReg.hots.length; i < l; i++) {
            let h = stateReg.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
                // We are inside click hot
                switch (h.row) {
                    case (stateReg.cpId):
                        // CP changed
                        if ((stateReg.disableCP === 0)) {
                            animateFallingCP(stateReg);
                        }
                        break;
                    case stateReg.addFlipId:
                        if (stateReg.disableCP === 0) {
                            let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                            let newNumOfFlips = calculateFlips(e, oldNumOfFlips);
                            let checkedFlipsCount = checkFlipsCount(stateReg, newNumOfFlips);
                            stateReg = updateState(stateReg, checkedFlipsCount);
                            resetDiagram();
                        }

                        break;
                    case stateReg.implId:
                        let foundRightClick = false;
                        if ("which" in e) {
                            if (e.which === 3) {
                                foundRightClick = true;
                            }
                        } else if (e.button === 2) {
                            foundRightClick = true;
                        }
                        if (foundRightClick) {
                            propRightClick(e, stateReg, h);
                            let oldNumOfFlips = stateReg.flipsCount.numOfFlips;
                            let checkedFlipsCount = checkFlipsCount(stateReg, oldNumOfFlips);
                            stateReg = updateState(stateReg, checkedFlipsCount);
                            resetDiagram();
                        }
                        break;
                    case stateReg.addDelayId:
                        if (stateReg.disableCP === 0) {
                            stateReg.countDelay++;
                            let delay = 500;
                            let factor = stateReg.countDelay;
                            if (factor === 5) {
                                stateReg.countDelay = 0;
                                factor = 0;
                            }
                            stateReg.msDelay = delay * factor;
                            resetDiagram();
                        }
                        break;
                    default:
                        alert("Impossible click!");
                }
                update();
                break;
            }
        }
    }

    function getArray(value, numOfElements) {
        let array = [];
        for (let i = numOfElements - 1; i >= 0; i--) {
            array.push(value);
        }
        return array;
    }

    function drawRegImpl(canvas, context, outerState, stateT, constants) {
        // Clear canvas

        let flipsC = outerState.flipsCount.numOfFlips;
        let dynamicClear;
        if (flipsC === 8) {
            dynamicClear = -30;
        } else {
            dynamicClear = -22
        }

        context.clearRect(0, 0, canvas.width, canvas.height / 2 + dynamicClear);
        writeSimulationName(canvas, context, stateT.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateT);
        clearStateHots(stateReg);

        // Init colors
        let colorsOut = getColorStyle(stateReg.Q, constants);
        let colorsLeft = getColorStyle([0, 0, stateReg.CP], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        // Inner D flip flops
        let numOfFlips = outerState.flipsCount.numOfFlips;
        let scaleD = constants.scaleFactor - 0.5;
        let constD = new Constants(scaleD);
        let flipDWidth = 50 * constD.scaleFactor;
        let flipDHeight = 67 * constD.scaleFactor;
        let dist = 85 * constD.scaleFactor;
        let holes = numOfFlips - 1;
        // Out box for holding D flip flops

        let boxWidth = dist * (holes + 1) + 2 * constants.inputLineLengthD +
            flipDWidth * numOfFlips * (constants.scaleFactor - 0.5);
        let boxHeight = 110 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let addSpace = 0;
        if (numOfFlips === 8) {
            addSpace += 35;
        }

        let yOff = -10 * constants.scaleFactor;
        let startH = 175;
        let addSpaceVertical = (numOfFlips * 8 * constants.scaleFactor);
        let regBox = {
            startX: (canvas.width - boxWidth) / 2 + xOff,
            startY: startH + yOff - addSpaceVertical * 0.6,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth + addSpace,
            boxHeight: boxHeight + addSpaceVertical,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            boxLineWidth: constants.boxLineWidth,
            lineColors: [colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            strokeWidth: constants.boxStrokeWidth,
            state: stateReg,
            flipFlopType: 1,
            // Indexes are: 0, 1, 2, 3:
            // 0 and 1 for left and right, 2 and 3 for up and down
            // 0 - Draw from left to right and from up to down, 1 inverse of 0.
            lineDirection: [0, 0, 1, 1],
            clickText: [
                getArray(-1, numOfFlips),
                [-1, -1, outerState.CP],
            ]
        };

        // Define Box Elements
        let textLeft = ["", "", "CP"];
        regBox.addLines = [
            getArray(1, numOfFlips),
        ];
        regBox.addValues = [
            getArray(1, numOfFlips),
        ];
        regBox.addClickBox = [
            getArray(false, numOfFlips),
        ];

        regBox.clickIds = [
            getArray(-1, numOfFlips),
        ];

        // Init text
        let letterOut = "Q";
        let unicodeSubScript0 = 8320;
        let textOut = getTextOut(letterOut, unicodeSubScript0, numOfFlips);

        regBox.text = [
            textOut,
            textLeft
        ];

        regBox.textPos = [
            getArray(0, numOfFlips),
        ];

        // addBoxElements(regBox, numOfFlips);
        let CPClickBox = [false, false, true];
        let CPValues = [0, 0, 1];
        let CPLines = [0, 0, 1];
        let CPTextPos = [0, 0, 0];
        let CPClickIds = [-1, -1, outerState.cpId];
        regBox.addValues.push(CPValues);
        regBox.addLines.push(CPLines);
        regBox.textPos.push(CPTextPos);
        regBox.addClickBox.push(CPClickBox);
        regBox.clickIds.push(CPClickIds);

        // Save click hots for view change
        let regBoxClickId = outerState.implId;
        saveClickHots(outerState, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let leftPosition = 0;
        let outputPosition = 2;
        let leftIndex = 1;
        let outLineIndex = 0;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        // Draw Inner Clock
        drawFlipLines(context, regBox, leftIndex, leftPosition, constants);

        // Draw D Flip Flops
        let jumpAhead = (regBox.boxWidth -
            (numOfFlips * flipDWidth + holes * dist)) / 2;
        let yOffInner = (regBox.boxHeight - (flipDHeight)) / 2;
        let flipDDistance = dist;
        dist += flipDWidth;

        // Draw inner D flip flops
        // Draw CP Line
        let yLineOff = getYLinesOutOff(regBox.boxHeight, 3);
        let innerCPLine = getLineParams(
            regBox.startX,
            regBox.startY + regBox.boxHeight - yLineOff,
            regBox.startX + jumpAhead + (dist * (numOfFlips - 1)) -
            constD.inputLineLengthD - 2 * constD.schemaComplementRadius,
            regBox.startY + regBox.boxHeight - yLineOff,
            constants.passiveStateColor
        );
        drawComplexLine(context, innerCPLine, colorsLeft[2],
            regBox.state, constD, 0);


        for (let c = numOfFlips - 1; c >= 0; c--) {
            let curD = {
                xCoord: regBox.startX + jumpAhead + (dist * c),
                yCoord: regBox.startY + (yOffInner) + addSpaceVertical / 2,
                state: stateT,
                boxWidth: flipDWidth,
                boxHeight: flipDHeight
            };
            drawT(c, context, regBox, curD, constD, flipDDistance);
        }

        // Draw decoder state
        drawDecodedState(context, regBox, regBox.state, constants);
    }


    function update() {
        if (stateReg.impl === 0) {
            drawReg(canvas, context, stateReg, constReg);
        } else {
            drawRegImpl(canvas, context, stateReg, stateT, constReg);
        }
    }

    function getColorStyle(listToCheck, constants) {
        let listLength = listToCheck.length;
        let colorsStlyes = [];
        for (let c = 0; c < listLength; c++) {
            if (listToCheck[c] === 1) {
                colorsStlyes.push(constants.greenLineActiveColor);
            } else if (listToCheck[c] === 0) {
                colorsStlyes.push(constants.passiveStateColor);
            }
        }
        return colorsStlyes;
    }

    function addClickAdder(context, box, constants) {
        let x, y, y2, x2;
        if (box.state.impl === 0) {
            x = 825;
            y = 20;
            y2 = 20;
            x2 = 20;
        } else {
            x = 825;
            y = 20;
            y2 = 20;
            x2 = 20;
        }
        let clickToAddDelay = {
            startX: x,
            startY: y,
            clickBoxWidth: 2 * constants.textBoxWidth,
            clickBoxHeight: 2 * constants.textBoxHeight,
            clickId: box.state.addDelayId,
            clickText: box.state.countDelay * 500 + " ms"
        };
        let clickToAddFlip = {
            startX: x2,
            startY: y2,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            clickId: box.state.addFlipId,
            clickText: box.state.flipsCount.numOfFlips
        };
        drawClickBox(context, clickToAddFlip, box.state, constants);
        drawClickBox(context, clickToAddDelay, box.state, constants);

    }

    function drawReg(canvas, context, state, constants) {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName, constants);
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);

        // Important state hots tasks
        clearStateHots(stateReg);

        // Init colors
        let colorsOut = getColorStyle(state.Q, constants);
        let colorsLeft = getColorStyle([state.CP], constants);
        let boxColors = {
            box: constants.colors.registerColor,
            clickBox: constants.colors.clickBoxColor
        };

        let numOfFlips = state.flipsCount.numOfFlips;
        let boxWidth = 40 + (numOfFlips * constants.clickBoxWidth) * constants.scaleFactor;
        let boxHeight = 60 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;

        let regBox = {
            startX: (canvas.width - boxWidth) / 2 + xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthNew,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            flipFlopType: 2,
            lineDirection: [0, 0, 1, 1],
            clickText: [
                getArray(-1, numOfFlips),
                [state.CP]
            ],
            lineColors: [colorsOut, colorsLeft],
            clickBoxColor: boxColors.clickBox,
            fillStyle: boxColors.box,
            lineWidth: constants.boxStrokeWidth,
            state: state
        };
        let textLeft = ["CP"];

        // Define Box Elements
        regBox.addLines = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addValues = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];
        regBox.addClickBox = [
            getArray(false, numOfFlips),
            getArray(true, 1),
        ];

        regBox.clickIds = [
            getArray(-1, numOfFlips),
            [state.cpId],
        ];

        // Init text
        let letterOut = "Q";
        let textOut = [];
        let unicodeSubScript0 = 8320;
        for (let c = 0; c < numOfFlips; c++) {
            // Make this generic...
            if (c < 10) {
                textOut.push(letterOut + String.fromCharCode(unicodeSubScript0 + c));
            } else if (c < 20) {
                let uni1 = 8321;
                let uni2 = unicodeSubScript0 + c - 10;
                textOut.push(letterOut +
                    String.fromCharCode(uni1) + String.fromCharCode(uni2));
            }
        }

        regBox.text = [
            textOut,
            textLeft
        ];

        regBox.textPos = [
            getArray(1, numOfFlips),
            getArray(1, 1)
        ];

        // Save click hots for view change
        let regBoxClickId = state.implId;
        saveClickHots(state, regBox.startX, regBox.startY,
            regBox.boxWidth, regBox.boxHeight, regBoxClickId);

        addClickAdder(context, regBox, constants);

        // Draw Register
        // Position values: 0 - left, 1 - right, 2 up, 3 down
        let outputPosition = 2;
        let clkPosition = 0;
        let outLineIndex = 0;
        let leftLineIndex = 1;
        drawFlipFlopBox(context, regBox, constants);
        drawFlipLines(context, regBox, outLineIndex,
            outputPosition, constants);
        drawFlipLines(context, regBox, leftLineIndex,
            clkPosition, constants);

        // Additional items

        // Draw decoder state
        drawDecodedState(context, regBox, regBox.state, constants);
    }

    function wheelEventHandling(e) {
        myMouseWheelEvent(e, canvas, zoomHotsReg);
        update();
    }

    function myMouseWheelEvent(e, canvas, hots) {
        e.preventDefault();
        mouseZoomer(e, canvas, hots, processZoom, stateReg.scaleFactor);
    }


    function propRightClick(e, state, h) {
        if (h.row === state.implId) {
            state.impl = (state.impl === 1) ? 0 : 1;
            e.preventDefault();
        }
    }

    function rightClickHot(e) {
        e.preventDefault();
    }


    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEvent2);
        canvas.addEventListener('mousewheel', wheelEventHandling, false);
        canvas.addEventListener('contextmenu', rightClickHot, false);

        // Remove dblclk on canvas to select text in document.
        canvas.addEventListener('selectstart',
            function (e) {
                e.preventDefault();
                return false;
            }, false);
        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', wheelEventHandling, false)

    }

    function init(canvas, context, state, constants) {
        if (state.impl === 0) {
            drawReg(canvas, context, state, constants);
        } else {
            drawRegImpl(canvas, context, stateReg, stateT, constReg);
        }
        // Init state diagram
        initStateDiagram(canvas, currentState, context, state, constants);
    }

    function getYLinesInnerOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }

    function getYLinesOutOff(availableLength, items) {
        let lineDistance = availableLength / items;
        return ((availableLength - (items - 1) *
        lineDistance) / 2);
    }

    function drawQToDLine(ID, context, outBox, args, constants) {
        let colorQ = getColorStyle([args.state.Q[ID]], constants);
        // let colorQM1 = getColorStyle([args.state.Q[ID - 1]], constants);
        let flipOffset = ((ID - 1) * 0.6 * 15) + 12 * constants.scaleFactor;
        // Draw output to Q of outer box

        // let yOff = 35 * constants.scaleFactor;
        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let offFromOuterBox = 10;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            outBox.startY + offFromOuterBox,
            constants.colors.transparentLinePassive
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[0].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[0].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.colors.transparentLinePassive
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.colors.transparentLinePassive
        );

        // let yOffCP = getYLinesOutOff(outBox.boxHeight, outBox.text[1].length);
        // let addVal = getCPComplementFix(2, constants);

        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);

        let numOfFlips = outBox.state.flipsCount.numOfFlips;
        if (ID == (numOfFlips - 1)) {
            return;
        }
        // calc output of AND
        let andIn1;
        if (ID === 1) {
            andIn1 = (args.state.Q[ID - 1] === 1) ? 1 : 0;
        } else {
            andIn1 = (args.state.T[ID] === 1) ? 1 : 0;
        }
        let thisAndOut = outBox.state.ands[ID - 1];

        // Draw serial connectors

        let andScale = 0.7;
        let flipLeftXOff = 15 * constants.scaleFactor;

        // Set up and values
        let colorsIn = getColorStyle([andIn1], constants);
        colorsIn.push(colorQ[0]);
        let colorsOut = getColorStyle([thisAndOut], constants);
        let and1 = {
            startX: outQUp.startX + flipLeftXOff,
            startY: args.startY - flipOffset - constants.NANDBoxHeight * andScale,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthD,
            boxWidth: constants.NANDBoxWidth * andScale + ((ID - 1) * 0.2 * 5),
            boxHeight: ((ID - 1) * 0.30 * constants.NANDBoxHeight) + constants.NANDBoxHeight * andScale,
            textPos: [[1, 1], [1]],
            addLines: [[0, 0], [0]],
            lineDirection: [0, 0, 0, 1],
            addValues: [[1, 1], [1]],
            addClickBox: [
                [false, false],
                [false, false]
            ],
            text: [["", ""], [""]],
            lineColors: [colorsIn, colorsOut],
            flipFlopType: 1,
            boxLineWidth: constants.boxLineWidth,
            clickBoxColor: constants.colors.clickBoxColor,
            fillStyle: constants.colors.andSyncColor,
            state: undefined,
        };

        let yAndLinesOff =
            getYLinesInnerOff(and1.boxHeight, ID + 1);

        let andLineDistance = and1.boxHeight / (ID + 1);
        let andInLast = getLineParams(
            outQUp.startX,
            and1.startY + yAndLinesOff + (andLineDistance * ID),
            outQUp.startX + flipLeftXOff,
            and1.startY + yAndLinesOff + (andLineDistance * ID),
            constants.passiveStateColor
        );

        drawComplexLine(context, andInLast, colorQ[0], args.state, constants, 0);


        let flipText = {
            startX: and1.startX,
            startY: and1.startY,
            boxWidth: and1.boxWidth,
            boxHeight: and1.boxHeight,
            fillStyle: constants.colors.black,
            color: constants.colors.black,
            textFont: constants.simTextFont,
            lineWidth: constants.textLineWidth,
        };

        if (outBox.state.calculatingAnd === 1 &&
            outBox.state.msDelay != 0) {
            let glowWidth = 3;
            drawGlowingBox(context, and1, glowWidth, constants);
        }

        drawFlipFlopBox(context, and1, constants);
        drawText(context, flipText, "&", true, true);

        // Draw values smaller..
        let constAnds = new Constants(constants.scaleFactor * 0.5);
        let o = {
            textLength: constants.valuesBoxWidth,
        };
        for (let c = 0; c <= ID; c++) {
            let colorQNth = getColorStyle([args.state.Q[c]], constants);
            let lineDistance = and1.boxHeight / (ID + 1);
            let startPoints = {
                startX: and1.startX,
                startY: and1.startY + yAndLinesOff + (c * lineDistance),
            };
            let valPosNth = getValueBox(startPoints, o, 0, constants);
            valPosNth.startY += (2.5 * constants.scaleFactor);


            drawValues(context, valPosNth, colorQNth[0], constAnds);
        }
        let valPos3 = {
            startX: and1.startX + and1.boxWidth,
            startY: and1.startY + and1.boxHeight / 2,
        };
        let valOut = getValueBox(valPos3, o, 1, constants);

        drawValues(context, valOut, thisAndOut, constAnds);

        let lineAndOut = getLineParams(
            and1.startX + and1.boxWidth,
            and1.startY + and1.boxHeight / 2,
            args.startX + args.boxWidth + args.flipDDistance - args.lineLength,
            and1.startY + and1.boxHeight / 2
        );

        let lineAndOutDown = getLineParams(
            lineAndOut.endX,
            lineAndOut.endY,
            lineAndOut.endX,
            args.startY + yLinesOff
        );
        drawComplexLine(context, lineAndOut, colorsOut[0], args.state, constants, 0);
        drawComplexLine(context, lineAndOutDown, colorsOut[0],
            args.state, constants, 0);


        if ((ID + 1) === outBox.state.flipsCount.numOfFlips - 1) {
            let numOfBehinds = ID;
            for (let c = 0; c < numOfBehinds; c++) {
                // Needed color
                let colorQNth = getColorStyle([args.state.Q[c]], constants);
                // Starting point of Q_nth

                let distanceFromTwoQs = (args.flipDDistance + args.boxWidth) * c;
                let startX = args.startX -
                    (numOfBehinds * args.flipDDistance + (ID - 1) * args.boxWidth) +
                    args.lineLength + distanceFromTwoQs;
                let startY = outBox.startY + 1.5 * offFromOuterBox + (c * 7);

                // Some ending point, like args.startX + off maybe
                let endPointDifference = -1 * (c * 5);
                let lineInNth = getLineParams(
                    startX,
                    startY,
                    args.startX + args.boxWidth + endPointDifference,
                    startY
                );
                let dotNth = {
                    centerX: lineInNth.startX,
                    centerY: lineInNth.startY,
                    radius: constants.IEEEConnectDotRadius,
                    startAngle: toRadians(constants.zeroAngle),
                    endAngle: toRadians(constants.fullCircle),
                    rotation: false,
                    color: constants.passiveStateColor,
                    fillStyle: constants.passiveStateColor,
                    lineWidth: constants.connectDotLineWidth
                };

                let lineDistance = and1.boxHeight / (ID + 1);
                // A line to lower the specific height to and input
                let lineInLower = getLineParams(
                    lineInNth.endX,
                    lineInNth.endY,
                    lineInNth.endX,
                    and1.startY + yAndLinesOff + (c * lineDistance)
                );

                // A line to connect line to input
                let lineInConnect = getLineParams(
                    lineInLower.endX,
                    lineInLower.endY,
                    and1.startX,
                    lineInLower.endY
                );

                drawComplexLine(context, lineInNth,
                    colorQNth[0], args.state, constants, 0);
                drawComplexLine(context, lineInLower,
                    colorQNth[0], args.state, constants, 0);
                drawComplexLine(context, lineInConnect,
                    colorQNth[0], args.state, constants, 0);
                drawCircle(context, dotNth);
            }
        } else {
            // Now we connect points which we need
            // draw and ins
            for (let c = 0; c < ID; c++) {
                // Needed color
                let colorQNth = getColorStyle([args.state.Q[c]], constants);

                // Starting point of Q_nth

                let endPointDifference = -1 * (c * 5);
                let startX = args.startX + args.boxWidth / 2 + endPointDifference;
                let startY = outBox.startY + 1.5 * offFromOuterBox + (c * 7);

                let lineDistance = and1.boxHeight / (ID + 1);
                // A line to lower the specific height to and input
                let lineInLower = getLineParams(
                    startX,
                    startY,
                    startX,
                    and1.startY + yAndLinesOff + (c * lineDistance)
                );

                let dotNth = {
                    centerX: lineInLower.startX,
                    centerY: lineInLower.startY,
                    radius: constants.IEEEConnectDotRadius,
                    startAngle: toRadians(constants.zeroAngle),
                    endAngle: toRadians(constants.fullCircle),
                    rotation: false,
                    color: constants.passiveStateColor,
                    fillStyle: constants.passiveStateColor,
                    lineWidth: constants.connectDotLineWidth
                };

                // A line to connect line to input
                let lineInConnect = getLineParams(
                    lineInLower.endX,
                    lineInLower.endY,
                    and1.startX,
                    lineInLower.endY
                );

                drawComplexLine(context, lineInLower,
                    colorQNth[0], args.state, constants, 0);
                drawComplexLine(context, lineInConnect,
                    colorQNth[0], args.state, constants, 0);

                drawCircle(context, dotNth);

            }
        }

    }

    function drawFirst(ID, context, outBox, args, constants) {
        let colorQ = getColorStyle([args.state.Q[ID]], constants);

        let andScale = 0.7;
        let yLinesOff = getYLinesInnerOff(args.boxHeight, args.text[1].length);
        let yOffFromOuter = 10;
        let outQUp = getLineParams(
            args.startX + args.boxWidth + args.lineLength,
            args.startY + yLinesOff,
            args.startX + args.boxWidth + args.lineLength,
            outBox.startY + yOffFromOuter,
            constants.colors.transparentLinePassive
        );

        let tIn = getLineParams(
            outQUp.startX,
            outQUp.startY,
            outQUp.startX + args.flipDDistance - args.lineLength,
            outQUp.startY,
            constants.passiveStateColor
        );

        let lineOuterDistance = outBox.boxWidth / outBox.text[0].length;
        let yLinesOutOff = getYLinesOutOff(outBox.boxWidth, outBox.text[0].length);
        let xOff = outBox.startX + yLinesOutOff + (ID) * lineOuterDistance;
        let outQLeft = getLineParams(
            outQUp.endX,
            outQUp.endY,
            xOff,
            outQUp.endY,
            constants.colors.transparentLinePassive
        );

        let outQUp2 = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            outBox.startY,
            constants.colors.transparentLinePassive
        );

        drawComplexLine(context, outQUp, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQLeft, colorQ[0], args.state, constants, 1);
        drawComplexLine(context, outQUp2, colorQ[0], args.state, constants, 1);


        let dotTIn = {
            centerX: outQUp.startX,
            centerY: outQUp.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

        let numOfFlips = outBox.state.flipsCount.numOfFlips;
        if (numOfFlips != 1) {
            drawComplexLine(context, tIn, colorQ[0], args.state, constants, 0);
            drawCircle(context, dotTIn);
        }
    }

    function drawT(ID, context, outBox, args, constants, dist) {

        // Init Colours
        stateT.T = outBox.state.T;
        stateT.CP = outBox.state.CP;
        stateT.Q = outBox.state.Q;
        stateT.nQ = outBox.state.nQ;
        stateT.impl = outBox.state.impl;

        let colorsIn;
        if (ID === 0) {
            outBox.state.T[ID] = 1;
            colorsIn = getColorStyle([1, stateT.CP], constants);
        } else if (ID === 1) {

            colorsIn = getColorStyle([stateT.Q[ID - 1], stateT.CP], constants);
        } else if (ID === 2) {
            // calc output of AND
            let andOut = (outBox.state.ands[ID - 2] === 1) ? 1 : 0;
            outBox.state.T[ID] = andOut;
            colorsIn = getColorStyle([andOut, stateT.CP], constants);
        } else {
            stateT.T[ID] = outBox.state.ands[ID - 2];
            colorsIn = getColorStyle([stateT.T[ID], stateT.CP], constants);
        }
        let colorsOut = getColorStyle([stateT.Q[ID], stateT.nQ[ID]], constants);
        let colorsBox = {
            box: constants.colors.flipT,
            clickBox: constants.colors.clickBoxColor
        };

        let boxWidth = args.boxWidth;
        let boxHeight = args.boxHeight;
        let xOff = args.xCoord;
        let yOff = args.yCoord;
        let genericBox = {
            startX: xOff,
            startY: yOff,
            inputLineLength: constants.inputLineLength,
            lineLength: constants.inputLineLengthD,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            clickIds: [
                [-1, -1],
                [-1, -1],
            ],
            textPos: [[1, 1], [1, 1]],
            addLines: [[1, 1], [1, 1]],
            lineDirection: [0, 0, 0, 1],
            addValues: [[1, 1], [1, 1]],
            addClickBox: [
                [false, false],
                [false, false]
            ],
            text: [["T", "CP"], ["Q", "Q'"]],
            lineColors: [colorsIn, colorsOut],
            flipFlopType: 2,
            boxLineWidth: constants.boxLineWidth,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: stateT,
            flipDDistance: dist

        };

        // let numOfFlips = outBox.state.flipsCount.numOfFlips;
        // Draw flip flop. Draw IO lines
        passiveStyle(context, constants);

        // See if flip flop is calculating
        if (outBox.state.calculatingT === 1 && outBox.state.msDelay != 0) {
            let glowWidth = 3;
            drawGlowingBox(context, genericBox, glowWidth, constants);
        }

        drawFlipFlopBox(context, genericBox, constants);
        drawFlipLines(context, genericBox, 0, 0, constants);
        drawFlipLines(context, genericBox, 1, 1, constants);
        //   drawCPBelow(context, genericBox, constants, genericBox.flipFlopType);
        if (ID === 0) {
            drawFirst(ID, context, outBox, genericBox, constants);
        } else {
            drawQToDLine(ID, context, outBox, genericBox, constants);
        }

        let yLineOffOut = getYLinesOutOff(outBox.boxHeight, 3);
        let yLineOff = getYLinesOutOff(genericBox.boxHeight, 2);
        let connectCP = getLineParams(
            genericBox.startX - genericBox.lineLength -
            2 * constants.schemaComplementRadius,
            genericBox.startY + genericBox.boxHeight - yLineOff,
            genericBox.startX - genericBox.lineLength -
            2 * constants.schemaComplementRadius,
            outBox.startY + outBox.boxHeight - yLineOffOut,
            constants.passiveStateColor
        );
        drawComplexLine(context, connectCP, colorsIn[1],
            genericBox.state, constants, 1);


        let dotCP = {
            centerX: connectCP.endX,
            centerY: connectCP.endY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

        drawCircle(context, dotCP);

        passiveStyle(context, constants);
    }

    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateFallingCP(state) {
        state.disableCP = 1;
        state.cpChanged = 1;
        state.CP = (state.CP === 1) ? 0 : 1;
        if (state.CP === 0) {
            state.CPEdge = 1;
        } else {
            state.CPEdge = 0;
        }
        if (state.CPEdge === 1) {
            state.calculatingT = 1;
            setTimeout(function () {
                state.calculatingT = 0;
                let flipLength = state.Q.length;
                let tempQ = [];
                let tempT = [];
                for (let c = 0; c < flipLength; c++) {
                    if (c === 0) {
                        state.T[c] = 1;
                    } else if (c === 1) {
                        state.T[c] = (state.Q[c - 1] === 1) ? 1 : 0;
                    } else {
                        state.T[c] = (state.ands[c - 2] === 1) ? 1 : 0;
                    }
                    tempT.push(state.T[c]);
                    tempQ.push(state.Q[c]);
                }

                for (let c = 0; c < flipLength; c++) {
                    // Set T and Q
                    let newQ = (tempT[c] === 1) ?
                        (tempQ[c] === 1) ? 0 : 1 : tempQ[c];
                    let newNQ = (newQ === 1) ? 0 : 1;

                    // Changed value of Q
                    state.valueChangeQ[c] = (state.Q[c] != newQ) ? 1 : 0;
                    state.Q[c] = newQ;
                    state.nQ[c] = newNQ;
                }
                if (state.flipsCount.numOfFlips <= 2) {
                    state.disableCP = 0;
                    return;
                }

                state.calculatingAnd = 1;
                setTimeout(function () {
                    let andLength = state.ands.length;
                    for (let c = 0; c < andLength; c++) {
                        let isTrue = true;
                        let numOfBehinds = c + 2;
                        while (numOfBehinds > 0) {
                            if (state.Q[numOfBehinds - 1] === 0) {
                                isTrue = false;
                            }
                            numOfBehinds -= 1;
                        }
                        state.ands[c] = (isTrue === true) ? 1 : 0;
                    }
                    state.calculatingAnd = 0;
                    state.disableCP = 0;
                }, state.msDelay)
            }, state.msDelay);
        } else {
            state.disableCP = 0;
        }
        state.CPEdge = 0;
    }

    // Diagram drawings

    let labelOff = 10;
    let delay = (stateReg.msDelay === 0) ? 1000 : stateReg.msDelay;
    let diagramHeight =
        ((stateReg.flipsCount.numOfFlips + 1) * (constReg.textBoxHeight + labelOff));
    let currentState = {
        id: 0,
        halfCanvas: canvas.height / 2 - 50,
        currentTime: 0,
        diagramHeight: diagramHeight,
        currentX: 0,
        startHeight: (canvas.height / 2) + 75 + diagramHeight,
        startX: 50,
        endX: 800,
        highLevel: 20,
        lowLevel: 0,
        diagramRefresh: Math.round(delay / constReg.textBoxWidth),
    };
    currentState.xAxisTimeLength = Math.ceil((currentState.endX - currentState.startX) / constReg.textBoxWidth);


    function initAxes(context, currentState, state, constants) {
        let numOfOuts = state.Q.length;
        let labelOffset = 10;
        let startHeight = currentState.startHeight - currentState.diagramHeight;
        let yAxe = getLineParams(
            currentState.startX,
            currentState.startHeight,
            currentState.startX,
            startHeight
        );

        let xAxe = getLineParams(
            currentState.startX,
            currentState.startHeight,
            currentState.endX + 8,
            currentState.startHeight
        );
        drawConnectLine(context, xAxe, constants.diagramLineWidth, 1);
        drawConnectLine(context, yAxe, constants.diagramLineWidth, 1);

        // Init text
        let leftOff = 6;
        let letterOut = "Q";
        let unicode0 = 8320;
        let numOfFlips = state.flipsCount.numOfFlips;
        let textOut = getTextOut(letterOut, unicode0, numOfFlips);
        for (let c = 0; c < numOfOuts; c++) {
            let textLabel = {
                startX: currentState.startX - constants.textBoxWidth - leftOff,
                startY: currentState.startHeight -
                ((c + 1) * constants.textBoxHeight) -
                ((c + 1) * labelOffset),
                boxWidth: constants.textBoxWidth,
                boxHeight: constants.textBoxHeight,
                fillStyle: constants.colors.darkBlue,
                color: constants.colors.darkBlue,
                textFont: constants.registerFont,
                lineWidth: constants.textLineWidth,
                textLine: textOut[c]
            };
            drawText(context, textLabel, textLabel.textLine, true, false);
        }

        let textLabel = {
            startX: currentState.startX - constants.textBoxWidth - leftOff,
            startY: currentState.startHeight -
            ((numOfOuts + 1) * constants.textBoxHeight) -
            ((numOfOuts + 1) * labelOffset),
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            fillStyle: constants.colors.darkBlue,
            color: constants.colors.darkBlue,
            textFont: constants.registerFont,
            lineWidth: constants.textLineWidth,
            textLine: "CP"
        };
        drawText(context, textLabel, textLabel.textLine, true, false);
    }


    function adjustState(context, currentState, height, constants) {
        let curLine = getLineParams(
            currentState.currentX + currentState.startX,
            height,
            currentState.currentTime + currentState.startX,
            height
        );

        drawConnectLine(context, curLine, constants.diagramLineWidth, 0);
    }

    function drawVerticalChange(context, currentState, start, end, constants) {
        if (start === end) {
            start -= currentState.highLevel;
            start -= 1;
            end += 1;
        } else {
            start += 1;
            end -= 1;
        }
        let verticalLine = getLineParams(
            currentState.currentTime + currentState.startX,
            start,
            currentState.currentTime + currentState.startX,
            end
        );

        drawConnectLine(context, verticalLine, constants.diagramLineWidth, 0);
    }


    function updateDiagram(canvas, context, currentState, state, constants) {
        // clear canvas
        setBackgroundFrame(context,
            canvas.width, canvas.height, constants);
        // Update each element drawn by time scale

        let elements = state.Q.length;
        let lowHeight = currentState.lowLevel;
        let highHeight = currentState.highLevel;
        let labelOff = 10;
        for (let c = 0; c <= elements; c++) {
            // Function to adjust changes
            let startHeight = currentState.startHeight -
                (c * constants.textBoxWidth) - (c * labelOff);

            if (c === 0) {
                startHeight -= 8;
            }

            let startVertical = startHeight;
            if (c === elements) {
                startHeight -= ((state.CP === 1) ? highHeight : lowHeight);
            } else {
                startHeight -= ((state.Q[c] === 1) ? highHeight : lowHeight);
            }
            let endVertical = startHeight;

            adjustState(context, currentState, startHeight, constants);

            if (state.valueChangeQ[c] === 1) {
                drawVerticalChange(context, currentState,
                    startVertical, endVertical, constants);
                state.valueChangeQ[c] = 0;
            } else if (state.cpChanged === 1 && c === elements) {
                drawVerticalChange(context, currentState,
                    startVertical, endVertical, constants);
                state.cpChanged = 0;
            }

        }

        // Update state
        if (currentState.currentTime === (currentState.endX - currentState.startX)) {
            resetDiagram();
        }
        currentState.currentX = currentState.currentTime;
        currentState.currentTime++;
    }


    function initTimeline(context, currentState, constants) {
        // First draw numbers on time line x axis

        let endTime = currentState.xAxisTimeLength;
        let letterOff = 0;
        let smallInc = 0;
        for (let c = 0; c < endTime; c++) {
            let curNum = {
                startX: currentState.startX +
                letterOff + (c * constants.textBoxWidth) + (smallInc),
                startY: currentState.startHeight,
                boxWidth: constants.textBoxWidth,
                boxHeight: constants.textBoxHeight,
                fillStyle: constants.colors.black,
                color: constants.colors.black,
                textFont: constants.registerFont,
                lineWidth: constants.textLineWidth,
                textLine: c + 1
            };
            let fillStyle = ((c % 2) === 0) ?
                constants.colors.diagramBackground1 :
                constants.colors.diagramBackground2;
            let rectangle = {
                startX: currentState.startX + (c * curNum.boxWidth),
                startY: currentState.startHeight - currentState.diagramHeight,
                boxWidth: curNum.boxWidth,
                boxHeight: currentState.diagramHeight,
                fillStyle: fillStyle,
                lineWidth: 0,
                strokeStyle: constants.colors.fullTransparent
            };

            drawRect(context, rectangle, constants);
            drawText(context, curNum, curNum.textLine, true, false);
        }


    }

    function initStateDiagram(canvas, currentState, context, state, constants) {
        // init axes
        //      - Time scale: 0, 1, 2, 3...
        //      - Labels for y: Q0 to Qn-1 and CLOCK (CP)

        // Function for time...
        //      - Initialises all elements: Q and Clock
        // Draw current timeLine
        /*  let timeLineOff = 15;
         let timeLine = getLineParams(
         currentState.currentTime + currentState.startX,
         currentState.startHeight - timeLineOff,
         currentState.currentTime + currentState.startX,
         currentState.startHeight - currentState.diagramHeight
         );

         // drawConnectLine(context, timeLine, constants.diagramLineWidth, 1);
         */
        initTimeline(context, currentState, constants);
        initAxes(context, currentState, state, constants);
        currentState.id = setInterval(function () {
            updateDiagram(canvas, context, currentState, state, constants);
        }, currentState.diagramRefresh);
    }


    function resetDiagram() {
        let labelOff = 10;
        let delay = (stateReg.msDelay === 0) ? 1000 : stateReg.msDelay;
        currentState.currentTime = 0;
        currentState.currentX = currentState.currentTime;
        currentState.diagramHeight = ((stateReg.flipsCount.numOfFlips + 1) *
        (constReg.textBoxHeight + labelOff));
        currentState.startHeight = (canvas.height / 2) + 75 + diagramHeight;
        currentState.xAxisTimeLength = Math.ceil((currentState.endX - currentState.startX) / constReg.textBoxWidth);
        currentState.diagramRefresh =
            Math.round(delay / constReg.textBoxWidth);
        if (stateReg.impl === 0) {
            context.clearRect(0, currentState.halfCanvas - 200, canvas.width, canvas.height);
        } else {
            context.clearRect(0, currentState.halfCanvas, canvas.width, canvas.height);
        }

        clearInterval(currentState.id);
        initStateDiagram(canvas, currentState, context, stateReg, constReg);

    }


    init(canvas, context, stateReg, constReg);
    eventListeners(canvas);
    setInterval(startSimulation, currentSpeed);
}